/******************************************************************************
** datasetreader.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __DATASETREADER_H__
#define __DATASETREADER_H__

#include "pubsubobject.h"
#include "readerqueueitem.h"
#include "uafieldtargetdatatype.h"
#include "uakeyvaluepair.h"
#include "uauadpdatasetreadermessagedatatype.h"
#include "uajsondatasetreadermessagedatatype.h"
#include <vector>

namespace PubSubBase
{

class ReaderGroup;
class PubSubStackQueue;
class DataSetReaderCallback;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for PubSubConnections.
*/
class PUBSUBBASE_EXPORT DataSetReader : public PubSubObject
{
    friend class ReaderGroup;
    UA_DISABLE_COPY(DataSetReader);

protected:
    DataSetReader();
    DataSetReader(
        ReaderGroup*                 pParent,
        OpcUa_DataSetReaderDataType* pConfigData,
        OpcUa_UInt32                 configIndex,
        OpcUa_Int32                  stackHandle);
    DataSetReader(
        ReaderGroup*                 pParent,
        OpcUa_DataSetReaderDataType* pConfigData);
    virtual ~DataSetReader();

public:
    UaStatus startUp();
    UaStatus shutDown();

    // #################################################
    // DataSetReader settings
    UaString name() const;
    void setName(const UaString& name);
    bool enabled() const;
    void setEnabled(bool bEnabled);
    UaVariant publisherId() const;
    void setPublisherId(const UaVariant& publisherId);
    OpcUa_UInt16 writerGroupId() const;
    void setWriterGroupId(OpcUa_UInt16 writerGroupId);
    OpcUa_UInt16 dataSetWriterId() const;
    void setDataSetWriterId(OpcUa_UInt16 dataSetWriterId);
    OpcUa_DataSetMetaDataType* pDataSetMetaData() const;
    OpcUa_DataSetFieldContentMask dataSetFieldContentMask() const;
    void setDataSetFieldContentMask(OpcUa_DataSetFieldContentMask dataSetFieldContentMask);
    OpcUa_Double messageReceiveTimeout() const;
    void setMessageReceiveTimeout(OpcUa_Double messageReceiveTimeout);
    OpcUa_UInt32 keyFrameCount() const;
    void setKeyFrameCount(OpcUa_UInt32 keyFrameCount);
    UaString headerLayoutUri() const;
    void setHeaderLayoutUri(const UaString& headerLayoutUri);
    PubSubHeaderLayout headerLayout() const;
    UaKeyValuePairs dataSetReaderProperties() const;
    void setDataSetReaderProperties(const UaKeyValuePairs& dataSetReaderProperties);
    // UADP Message Settings
    UaStatus messageSettingsUadp(UaUadpDataSetReaderMessageDataType& uadpMessageSettings) const;
    UaStatus setMessageSettingsUadp(const UaUadpDataSetReaderMessageDataType& uadpMessageSettings);
    // JSON Message Settings
    UaStatus messageSettingsJson(UaJsonDataSetReaderMessageDataType& jsonMessageSettings) const;
    UaStatus setMessageSettingsJson(const UaJsonDataSetReaderMessageDataType& jsonMessageSettings);
    // #################################################

    // #################################################
    // Subscribed DataSet settings
    // Target Variables
    OpcUa_TargetVariablesDataType* pTargetVariables();
    UaStatus targetVariables(UaFieldTargetDataTypes& targetVariables);
    void setTargetVariables(UaFieldTargetDataTypes& targetVariables, bool detach = true);
    // DataSet Mirror
    OpcUa_SubscribedDataSetMirrorDataType* pSubscribedDataSetMirror();
    // #################################################

    // #################################################
    // Custom DataSetMessage processing through callback
    inline bool messageDecodedByApplication() const { return m_messageDecodedByApplication; }
    inline void setMessageDecodedByApplication(bool messageDecodedByApplication) { m_messageDecodedByApplication = messageDecodedByApplication; }
    void setDataSetReaderCallback(DataSetReaderCallback* pDataSetReaderCallback);
    inline DataSetReaderCallback* pDataSetReaderCallback() const { return m_pDataSetReaderCallback; }
    // #################################################

    // #################################################
    // Default DataSetMessage processing through queues
    void connectStackQueue(PubSubStackQueue* pStackQueue);
    OpcUa_UInt32 getQueueSize() const;
    ReaderQueueItem* getQueueItem(OpcUa_UInt32 index);
    // #################################################

    inline ReaderGroup* pParent() const { return m_pParent; }
    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_DataSetReaderDataType* pConfigData() const { return m_pConfigData; }

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_DataSetReader; }

private:
    void setConfigRelation(OpcUa_DataSetReaderDataType* pConfigData, OpcUa_UInt32 configIndex);
    static OpcUa_DataSetReaderDataType* createStructure();
    static UaStatus createDataSetReader(
        OpcUa_DataSetReaderDataType* pDataSetReader,
        const UaString&              name,
        bool                         enabled,
        const UaVariant&             publisherId,
        OpcUa_Double                 messageReceiveTimeout,
        OpcUa_Double                 receiveOffset,
        OpcUa_Double                 processingOffset,
        OpcUa_WriterGroupDataType*   pWriterGroup,
        OpcUa_DataSetWriterDataType* pDataSetWriter,
        OpcUa_DataSetMetaDataType*   pDataSetMetaData);

private:
    ReaderGroup*                 m_pParent;
    OpcUa_DataSetReaderDataType* m_pConfigData;
    OpcUa_UInt32                 m_configIndex;
    DataSetReaderCallback*       m_pDataSetReaderCallback;
    std::vector<ReaderQueueItem*> m_queueItems;
    bool                         m_messageDecodedByApplication;
};
/*! @} */

}

#endif // #ifndef __DATASETREADER_H__

