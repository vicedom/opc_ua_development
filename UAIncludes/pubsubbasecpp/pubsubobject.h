/******************************************************************************
** pubsubobject.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBOBJECT_H__
#define __PUBSUBOBJECT_H__

#include "pubsubbase.h"
#include "referencecounter.h"
#include "uamutex.h"
#include "userdatabase.h"

namespace PubSubBase
{

class PUBSUBBASE_EXPORT PubSubResources;

#define PUBSUB_INVALID_STACKHANDLE (OpcUa_Int32)0xFFFFFFFF

/** Base class for PubSub management objects.
*/
class PUBSUBBASE_EXPORT PubSubObject : public ReferenceCounter
{
    UA_DISABLE_COPY(PubSubObject);
protected:
    PubSubObject();
    virtual ~PubSubObject();
public:

    /** PubSub transport facet reflecting the defined TransportProfileUris */
    enum PubSubObjectType
    {
        PubSubObjectType_Configuration,
        PubSubObjectType_PublishedDataSet,
        PubSubObjectType_Connection,
        PubSubObjectType_WriterGroup,
        PubSubObjectType_DataSetWriter,
        PubSubObjectType_ReaderGroup,
        PubSubObjectType_DataSetReader
    };

    /** PubSub transport facet reflecting the defined TransportProfileUris */
    enum TransportFacet
    {
        TransportFacet_None             = 0x0,   /*!< No transport facet set. */
        TransportFacet_PubSub_UDP_UADP  = 0x11 , /*!< UDP transport protocol mapping with UADP message mapping. */
        TransportFacet_PubSub_ETH_UADP  = 0x21,  /*!< Ethernet transport protocol mapping with UADP message mapping. */
        TransportFacet_PubSub_AMQP_UADP = 0x31,  /*!< AMQP transport protocol mapping with UADP message mapping. */
        TransportFacet_PubSub_MQTT_UADP = 0x41   /*!< MQTT transport protocol mapping with UADP message mapping. */
    };

    /** PubSub configuration object state */
    enum ConfigurationState
    {
        ConfigurationState_None,             /*!< No configuration. */
        ConfigurationState_Deleted,          /*!< Object is ready for deletion. */
        ConfigurationState_InSyncWithStruct, /*!< Configuration in sync with current configuration structure. */
        ConfigurationState_New,              /*!< New configuration. */
        ConfigurationState_Modified          /*!< The configuration is modified. */
    };

    /** PubSub header configuration */
    enum PubSubHeaderLayout
    {
        PubSubHeaderLayout_Custom,            /*!< Custom configuration specific settings. */
        PubSubHeaderLayout_UADP_PeriodicFixed, /*!< Default configuration for peridic fixed layout DataSet messages. */
        PubSubHeaderLayout_UADP_Dynamic       /*!< Default configuration for dynamic layout DataSet messages. */
    };

    PubSubObject(PubSubResources* pPubSubResources, TransportFacet transportFacet, ConfigurationState configurationState);

    virtual PubSubObjectType pubSubObjectType() const = 0;

    inline PubSubResources* pPubSubResources() const { return m_pPubSubResources; }
    inline OpcUa_Int32 stackHandle() const { return m_stackHandle; }
    inline TransportFacet transportFacet() const { return m_transportFacet; }
    inline ConfigurationState configurationState() const { return m_configurationState; }
    inline void setConfigurationState(ConfigurationState configurationState) { m_configurationState = configurationState; }
    UaMutex* pConfigMutex() const;
    void incrementErrorCounter();
    void decrementErrorCounter();
    inline int getErrorCounter() const { return m_errorCounter; }

    // User Data
    void setUserData(UserDataBase *pUserData);
    inline UserDataBase *getUserData() const { return m_pUserData; }

protected:
    static OpcUa_UInt32 getEncodingId(OpcUa_ExtensionObject& obj);

protected:
    PubSubResources* m_pPubSubResources;
    OpcUa_Int32      m_stackHandle;
    TransportFacet   m_transportFacet;
    ConfigurationState m_configurationState;
    UserDataBase*    m_pUserData;
    int              m_errorCounter;
};

}

#endif // #ifndef __PUBSUBOBJECT_H__

