/******************************************************************************
** pubsubresources.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBRESOURCES_H__
#define __PUBSUBRESOURCES_H__

#include "pubsubbase.h"
#include "pubsubcontrolcallback.h"
#include "pubsubnetworkbackendinterface.h"
#include "referencecounter.h"
#include "uamutex.h"
#include <list>
#include <map>
#include "uapubsub_api.h"
UA_BEGIN_EXTERN_C
#include "uapubsub_samplingqueue.h"
#include "platform/memory.h"
UA_END_EXTERN_C

namespace PubSubBase
{

class DataSetWriter;

/** Class managing PubSub resources like stack and configurations.
*/
class PUBSUBBASE_EXPORT PubSubResources :
    public ReferenceCounter,
    public UaThread
{
    UA_DISABLE_COPY(PubSubResources);
protected:
    virtual ~PubSubResources();
public:
    PubSubResources();

    UaStatus loadConfiguration(const UaString& sConfigurationFile, bool initializeStack);
    UaStatus loadConfiguration(const UaByteString& bsConfiguration, bool initializeStack);
    UaStatus reallocStructures(
        OpcUa_UInt32 connectionCount,
        const std::list<OpcUa_UInt32>& deleteIndexListConnections,
        OpcUa_UInt32 dataSetCount,
        const std::list<OpcUa_UInt32>& deleteIndexListDataSets);
    UaStatus saveConfiguration(const UaString& sConfigurationFile);
    UaStatus saveConfiguration(UaByteString& bsConfiguration);
    static UaStatus saveConfiguration(const UaString& sConfigurationFile, OpcUa_PubSubConfigurationDataType* pConfiguration, const UaStringArray& namespaces);
    static UaStatus saveConfiguration(UaByteString& bsConfiguration, OpcUa_PubSubConfigurationDataType* pConfiguration, const UaStringArray& namespaces);
    UaStatus init();
    void clear();

    UaStatus startUp();
    UaStatus shutDown();

    inline UaMutex* pMutex() const { return m_pMutex; }
    inline PubSubControlCallback* pPubSubControlCallback() const { return m_pPubSubControlCallback; }
    inline void setPubSubControlCallback(PubSubControlCallback* pPubSubControlCallback) { m_pPubSubControlCallback = pPubSubControlCallback; }
    inline struct pubsub_api* pPubSubStack() const { return m_pPubSubStack; }
    inline OpcUa_PubSubConfigurationDataType* pLatestConfiguration() const { return m_pLatestConfiguration; }
    inline UaStringArray namespaces() const { return m_namespaces; }
    void setNamespaces(const UaStringArray& namespaces);
    UaStatus mapNamespaceTable(const UaStringArray& newNamespaceTable);
    inline pubsub_handle hPubSubModule() const { return m_hPubSubModule; }
    inline bool isConfigAvailable() const { return m_isConfigAvailable; }
    inline bool isStackInitialized() const { return m_isStackInitialized; }
    inline bool isStackStarted() const { return m_isStackStarted; }
    inline bool isDirty() const { return m_isDirty; }
    inline void setDirty(bool isDirty) { m_isDirty = isDirty; }

    static int pubsubStackDispatchFn(pubsub_handle obj, enum pubsub_cb_type type, pubsub_cb *old_cb, void **old_arg);
    static void registerDispatcher(struct pubsub_api* pPubSubApi);
    void setUserDataForStackObject(pubsub_handle handle, PubSubObject* pPubSubObject);
    void setPubSubNetworkBackend(PubSubNetworkBackendInterface* pNetworkBackend);
    PubSubNetworkBackendInterface* getPubSubNetworkBackend(PubSubNetworkBackendInterface::PubSubNetworkBackendInterfaceType interfaceType);

protected:
    // UaThread --------------------------
    void run();
    // UaThread --------------------------

private:
    UaMutex*                           m_pMutex;
    PubSubControlCallback*             m_pPubSubControlCallback;
    struct pubsub_api*                 m_pPubSubStack;
    pubsub_handle                      m_hPubSubModule;
    OpcUa_PubSubConfigurationDataType* m_pLatestConfiguration;
    UaStringArray                      m_namespaces;
    bool                               m_isConfigAvailable;
    bool                               m_isStackInitialized;
    bool                               m_isStackStarted;
    bool                               m_isDirty;
    bool                               m_stopThread;
    PubSubNetworkBackendInterface*     m_pNetworkBackendUdp;
    PubSubNetworkBackendInterface*     m_pNetworkBackendEth;
    PubSubNetworkBackendInterface*     m_pNetworkBackendMqtt;
    PubSubNetworkBackendInterface*     m_pNetworkBackendAmqp;
    PubSubNetworkBackendInterface*     m_pNetworkBackendUser;
    static struct pubsub_api*          s_pPubSubStack;
};

class PubSubStackQueue
{
public:
    struct pubsub_datavalue_samplingitem *queue;
    unsigned int queue_size;
};

}

#endif // #ifndef __PUBSUBRESOURCES_H__

