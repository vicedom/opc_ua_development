/******************************************************************************
** pubsubconnection.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBCONNECTION_H__
#define __PUBSUBCONNECTION_H__

#include "pubsubobject.h"
#include "writergroup.h"
#include "readergroup.h"
#include "uakeyvaluepair.h"
#include <list>

namespace PubSubBase
{

class PUBSUBBASE_EXPORT PubSubConfiguration;
class PUBSUBBASE_EXPORT PubSubNetworkBackendInterface;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for PubSubConnections.
*/
class PUBSUBBASE_EXPORT PubSubConnection : public PubSubObject
{
    friend class PubSubConfiguration;
    UA_DISABLE_COPY(PubSubConnection);

protected:
    PubSubConnection();
    PubSubConnection(
        PubSubConfiguration*            pParent,
        TransportFacet                  transportFacet,
        OpcUa_PubSubConnectionDataType* pConfigData,
        OpcUa_UInt32                    configIndex,
        OpcUa_Int32                     stackHandle);
    PubSubConnection(
        PubSubConfiguration*            pParent,
        TransportFacet                  transportFacet,
        OpcUa_PubSubConnectionDataType* pConfigData);
    virtual ~PubSubConnection();

public:
    UaStatus startUp();
    UaStatus shutDown();

    // #################################################
    // PubSubConnection settings
    UaString name() const;
    void setName(const UaString& name);
    bool enabled() const;
    void setEnabled(bool bEnabled);
    UaVariant publisherId() const;
    void setPublisherId(const UaVariant& publisherId);
    UaString transportProfileUri() const;
    UaString addressUrl() const;
    void setAddressUrl(const UaString& addressUrl);
    UaString addressNetworkInterface() const;
    void setAddressNetworkInterface(const UaString& addressNetworkInterface);
    UaKeyValuePairs connectionProperties() const;
    void setConnectionProperties(const UaKeyValuePairs& connectionProperties);
    // Datagram Transport Protocol
    UaString discoveryAddressUrl() const;
    void setDiscoveryAddressUrl(const UaString& discoveryAddressUrl);
    UaString discoveryAddressNetworkInterface() const;
    void setDiscoveryAddressNetworkInterface(const UaString& discoveryAddressNetworkInterface);
    // Broker Transport Protocol
    UaStatus transportSettingsBroker(OpcUa_BrokerConnectionTransportDataType& brokerTransportConfig) const;
    // #################################################

    // #################################################
    // Children handling
    WriterGroup* addWriterGroup(PubSubHeaderLayout headerLayout);
    WriterGroup* addWriterGroup_UADP(
        const UaString&            name,
        bool                       enabled,
        OpcUa_UInt16               writerGroupId,
        OpcUa_Double               publishingInterval,
        OpcUa_Double               keepAliveTime,
        PubSubHeaderLayout         headerLayout,
        OpcUa_Double               publishingOffset = -1,
        OpcUa_Double               samplingOffset = -1,
        OpcUa_UInt32               maxNetworkMessageSize = 1400);
    OpcUa_UInt32 writerGroupCount() const;
    WriterGroup* getWriterGroup(OpcUa_UInt32 index);
    UaStatus removeWriterGroup(WriterGroup* pGroup);

    ReaderGroup* addReaderGroup();
    ReaderGroup* addReaderGroup(
        const UaString& name,
        bool            enabled,
        OpcUa_UInt32    maxNetworkMessageSize = 1400);
    OpcUa_UInt32 readerGroupCount() const;
    ReaderGroup* getReaderGroup(OpcUa_UInt32 index);
    UaStatus removeReaderGroup(ReaderGroup* pGroup);
    // #################################################

    inline PubSubConfiguration* pParent() const { return m_pParent; }
    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_PubSubConnectionDataType* pConfigData() const { return m_pConfigData; }

    void setPubSubNetworkBackendInterface(PubSubNetworkBackendInterface* pPubSubNetworkBackendInterface);
    inline PubSubNetworkBackendInterface* pPubSubNetworkBackendInterface() const { return m_pPubSubNetworkBackendInterface; }

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_Connection; }

private:
    UaStatus createChildren();
    void initChildren();
    void setConfigRelation(OpcUa_PubSubConnectionDataType* pConfigData, OpcUa_UInt32 configIndex);
    UaStatus updateConnectionStructure();
    UaStatus reallocStructures(
        OpcUa_UInt32                   writerGroupCount,
        const std::list<OpcUa_UInt32>& deleteIndexListWriterGroups,
        OpcUa_UInt32                   readerGroupCount,
        const std::list<OpcUa_UInt32>& deleteIndexListReaderGroups);
    static OpcUa_PubSubConnectionDataType* createStructure();
    static UaStatus createConnection_Datagram(
        OpcUa_PubSubConnectionDataType* connection,
        const UaString&  name,
        bool             enabled,
        const UaVariant& publisherId,
        const UaString&  address,
        const UaString&  networkInterface,
        const UaString&  discoveryAddress,
        TransportFacet   transportFacet = PubSubObject::TransportFacet_PubSub_UDP_UADP);
    static UaStatus createConnection_Broker(
        OpcUa_PubSubConnectionDataType* connection,
        const UaString&  name,
        bool             enabled,
        const UaVariant& publisherId,
        const UaString&  address,
        TransportFacet   transportFacet);

private:
    PubSubConfiguration*            m_pParent;
    OpcUa_PubSubConnectionDataType* m_pConfigData;
    OpcUa_UInt32                    m_configIndex;
    PubSubNetworkBackendInterface*  m_pPubSubNetworkBackendInterface;
    std::list<WriterGroup*>         m_writerGroups;
    std::list<ReaderGroup*>         m_readerGroups;
};
/*! @} */

}

#endif // #ifndef __PUBSUBCONNECTION_H__

