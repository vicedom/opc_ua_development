/******************************************************************************
** pubsubmanager.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBMANAGER_H__
#define __PUBSUBMANAGER_H__

#include "coremoduleincludes.h"
#include <list>

#include "pubsubmodule.h"
#include "pubsubserverapplicationcallback.h"
#include "uaserverapplicationmodule.h"

class ServerManager;
class PubSubManagerPrivate;
class NodeAccessInfo;
namespace PubSubBase
{
    class PubSubNetworkBackendInterface;
}


/*! \addtogroup UaPubSubServerModule
*  @{
*/

/** Management class for server SDK OPC UA publish subscriber module.
*/
class SERVER_PUBSUBMODULE_EXPORT PubSubManager :
    public UaServerApplicationModule
{
    UA_DISABLE_COPY(PubSubManager);
public:
    PubSubManager();
    virtual ~PubSubManager();

    UaStatus setPubSubConfig(const UaString& configurationFile, const UaString& sConfigPath);
    UaStatus startUp(ServerManager* pServerManager);
    UaStatus shutDown();
    UaStatus reloadPubSubConfig();

    void setPubSubServerApplicationCallback(PubSubServerApplicationCallback* pCallback);
    void setConfigurationPermissions(NodeAccessInfo* pDefaultPermissions);
    void setPubSubNetworkBackendUser(PubSubBase::PubSubNetworkBackendInterface* pNetworkBackendUser);

private:
    PubSubManagerPrivate* d;
};
/*! @} */

#endif // #ifndef __PUBSUBMANAGER_H__

