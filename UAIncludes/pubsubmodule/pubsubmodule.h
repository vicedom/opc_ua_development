/******************************************************************************
** pubsubmodule.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.5
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.5, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.5/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBMODULE_H__
#define __PUBSUBMODULE_H__

#ifdef _WIN32
# ifdef _SERVER_PUBSUBMODULE_BUILD_DLL
#  define SERVER_PUBSUBMODULE_EXPORT __declspec(dllexport)
# elif defined _SERVER_PUBSUBMODULE_USE_DLL
#  define SERVER_PUBSUBMODULE_EXPORT __declspec(dllimport)
# else
#  define SERVER_PUBSUBMODULE_EXPORT
# endif
#else
# define SERVER_PUBSUBMODULE_EXPORT
#endif

#include <opcua_platformdefs.h>
#include "pubsubbase.h"

#endif // __PUBSUBMODULE_H__

