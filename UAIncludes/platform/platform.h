/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_FRONTEND_PLATFORM_H
#define UA_PLATFORM_FRONTEND_PLATFORM_H

#include <pplatform.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup platform_platform platform
 * @ingroup platform
 * General platform specfic defines and initialization/cleanup
 * for platform layer.
 * @{
 */

/** Panic for stopping applications in fatal error scenarios */
#define UA_PANIC(msg) ua_p_platform_panic(msg, __FILE__, __LINE__)

int ua_platform_init(void);
int ua_platform_cleanup(void);

/** @} */

UA_END_EXTERN_C

#endif /* end of include guard: UA_PLATFORM_FRONTEND_PLATFORM_H */

