/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_FRONTEND_SHUTDOWN_H
#define UA_PLATFORM_FRONTEND_SHUTDOWN_H

#include <platform/platform.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup platform_shutdown shutdown
 * @ingroup platform
 *
 * Functionality for server shutdown and configuration reload.
 *
 * Applications can use this to handle shutdown events and reload events.
 * Note: @ref ua_shutdown_register_handler is not called by @ref ua_platform_init,
 * because it is an application decision to handle these events.
 *
 * Applications must call them manually if they want to use this functionality:
 * @code
 * int main(void)
 * {
 *      ua_platform_init();
 *      ua_shutdown_register_handler();
 *
 *      // main server loop
 *
 *      ua_shutdown_unregister_handler();
 *      ua_platform_cleanup();
 *
 *      return 0;
 * }
 * @endcode
 *
 * Plaforms which don't support this should simply return 0 in all functions and
 * make this operations a NOP.
 *
 * @{
 */

int ua_shutdown_register_handler(void);
int ua_shutdown_unregister_handler(void);
int ua_shutdown_request_shutdown(void);
int ua_shutdown_should_shutdown(void);
int ua_shutdown_should_reload(void);

/** @} */

UA_END_EXTERN_C

#endif /* end of include guard: UA_PLATFORM_FRONTEND_SHUTDOWN_H */

