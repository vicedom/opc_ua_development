/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#define UA_NET_P_LASTSOCKETERROR        WSAGetLastError()
#define UA_NET_P_SOCKWRITE(x, y, z)     send(x, y, (int)z, 0)
#define UA_NET_P_SOCKREAD(x, y, z)      recv(x, y, (int)z, 0)
#define UA_NET_P_SOCKSELECT(nfds, readfds, writefds, exceptfds, timeout) select(nfds, readfds, writefds, exceptfds, timeout)
#define UA_NET_P_SOCKSENDTO(sockfd, buf, len, flags, dest_addr, addrlen)  sendto(sockfd, buf, len, flags, dest_addr, addrlen)
#define UA_NET_P_SOCKRECVFROM(sockfd, buf, len, flags, src_addr, addrlen) recvfrom(sockfd, buf, len, flags, src_addr, addrlen)
#define UA_NET_P_SOCKCLOSE(x)           closesocket(x)
#define UA_NET_P_SHUTDOWN_SEND          1
#define UA_NET_P_SHUTDOWN_READ          0
#define UA_NET_P_THREADSLEEP(x)         Sleep(x)
#define UA_NET_P_GETTICKS               GetTickCount
#define UA_NET_P_MAX_FD(x, y)           0
#define UA_NET_P_MAX_OF(x, y)           max(x, y)
#define UA_NET_P_MIN_OF(x, y)           min(x, y)
#define UA_NET_P_FD_ISSET(sockfd, fds)  ((sockfd) != UA_NET_P_SOCKET_INVALID)?FD_ISSET(sockfd, fds):0

/* some MSVCs have these defines, but it is not sure if there are identical to the expected WSA codes. */
#ifdef EWOULDBLOCK
# undef EWOULDBLOCK
#endif
#define EWOULDBLOCK                  WSAEWOULDBLOCK
#ifdef EINPROGRESS
# undef EINPROGRESS
#endif
#define EINPROGRESS                  WSAEINPROGRESS
#ifdef EINTR
# undef EINTR
#endif
#define EINTR                        WSAEINTR
#ifdef EAI_OVERFLOW
# undef EAI_OVERFLOW
#endif
#define EAI_OVERFLOW                 ERROR_INSUFFICIENT_BUFFER
#ifdef EMSGSIZE
# undef EMSGSIZE
#endif
#define EMSGSIZE                     WSAEMSGSIZE

#include <winsock2.h>
#include <ws2tcpip.h>

#ifdef UA_NET_SUPPORT_IPV6
#include <Mstcpip.h>
#else /* UA_NET_SUPPORT_IPV6 */
#define socklen_t int
#endif /* UA_NET_SUPPORT_IPV6 */

#include <windows.h>

typedef unsigned long in_addr_t;

static inline int ua_p_socket_set_non_blocking(struct ua_socket *a_pSocket)
{
    int iFlags  = 1;
    return ioctlsocket(a_pSocket->hSocket, FIONBIO, (u_long *)&iFlags);
}

static inline int ua_p_socket_set_reuse_address(struct ua_socket *a_pSocket)
{
    int iVal = 1;

    return (setsockopt(a_pSocket->hSocket, SOL_SOCKET, SO_REUSEADDR, (const char *)&iVal, sizeof(iVal)))?UA_NET_E_BADSYSCALL:UA_NET_E_GOOD;
}

static inline int ua_p_socket_disable_nagle(struct ua_socket *a_pSocket)
{
    int flag = 1;

    return (setsockopt(a_pSocket->hSocket, IPPROTO_TCP, TCP_NODELAY, (const char *)&flag, sizeof(flag)))?UA_NET_E_BADSYSCALL:UA_NET_E_GOOD;
}

static inline int ua_p_socket_join_group(struct ua_socket *a_pSocket, const char *a_pAddress, const char *a_pInterface)
{
    struct ip_mreq mreq;
    in_addr_t addr = (in_addr_t)-1;

    ua_memset(&mreq, 0, sizeof(mreq));

    if (a_pSocket == NULL || a_pAddress == NULL || a_pSocket->type != ua_socket_type_dgram) return UA_NET_E_BADINVALIDARGUMENT;

    /* Try to convert interface address */
    if (a_pInterface) {
        addr = inet_addr(a_pInterface);
    }

    if (addr != (in_addr_t)-1) {
        mreq.imr_interface.s_addr = addr;
    } else {
        mreq.imr_interface.s_addr = INADDR_ANY;
    }
    /* Try to convert group address */
    addr = inet_addr(a_pAddress);
    if (addr == (in_addr_t)-1) return UA_NET_E_BADINVALIDARGUMENT;
    mreq.imr_multiaddr.s_addr = addr;
    /* Join group */
    if (setsockopt(a_pSocket->hSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq)) < 0) {
        return UA_NET_E_BAD;
    }
    return UA_NET_E_GOOD;
}

static inline int ua_p_net_clear(void)
{
    WSACleanup();
    return UA_NET_E_GOOD;
}

static inline int ua_p_net_init(void)
{
    int iRes = 0;
    WSADATA wsaData;
    iRes = WSAStartup(MAKEWORD(2, 2), &wsaData);
    return iRes;
}
