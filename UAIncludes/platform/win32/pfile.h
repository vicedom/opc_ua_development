/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef PFILE_H_CO4E76KQ
#define PFILE_H_CO4E76KQ

#include <stdbool.h>
#include <pplatform.h> /* size_t */
#include <pfile_types.h>
#include <platform/file.h>

void ua_p_file_init(void);

int ua_p_file_open(const char *filename, int flags, ua_file_t *file);
int ua_p_file_close(ua_file_t file);
int ua_p_file_flush(ua_file_t file);
int ua_p_file_fstat(ua_file_t fd, struct ua_file_stat *buf);
int ua_p_file_stat(const char *filename, struct ua_file_stat *buf);
int ua_p_file_read(ua_file_t file, void *ptr, size_t count, size_t *bytes_read);
int ua_p_file_write(ua_file_t file, const void *ptr, size_t count, size_t *bytes_written);
long ua_p_file_seek(ua_file_t file, long offset, enum ua_file_whence whence);
long ua_p_file_tell(ua_file_t file);

int ua_p_file_scandir(const char *parent_dir, struct ua_dir_entry **results, filter_fct filter, sort_fct sort);
int ua_p_file_delete(const char *filename);
int ua_p_file_rmdir(const char *filename);
int ua_p_file_mkdir(const char *filename);
int ua_p_file_chdir(const char *filename);
int ua_p_file_getcwd(char *filename, size_t len);
bool ua_p_file_path_is_absolute(const char *path);

char ua_p_file_convert_newline(char c, bool *skip);

#endif /* end of include guard: PFILE_H_CO4E76KQ */

