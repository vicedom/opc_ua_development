/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_ATOMIC_H
#define UA_ATOMIC_H

#ifdef _MSC_VER
# define inline __inline
#endif

#include <stdbool.h>
#ifndef WIN32_LEAN_AND_MEAN
/* Always define WIN32_LEAN_AND_MEAN to prevent windows.h from including
 * winsock.h which leads to conflicts with winsock2.h */
# define WIN32_LEAN_AND_MEAN
#endif /* WIN32_LEAN_AND_MEAN */
#include <windows.h>

typedef struct {
    unsigned int counter;
} ua_atomic_t;

typedef struct {
    int lock;
} ua_lock_t;

static inline void ua_atomic_set(ua_atomic_t *v, unsigned int value)
{
    v->counter = value;
}

static inline unsigned int ua_atomic_inc(ua_atomic_t *v)
{
    return InterlockedIncrement((LONG*)&v->counter);
}

static inline unsigned int ua_atomic_dec(ua_atomic_t *v)
{
    return InterlockedDecrement((LONG*)&v->counter);
}

static inline unsigned int ua_atomic_add_return(unsigned int i, ua_atomic_t *v)
{
    InterlockedExchangeAdd((LONG*)&v->counter, i);
    return v->counter;
}

static inline unsigned int ua_atomic_sub_return(unsigned int i, ua_atomic_t *v)
{
    InterlockedExchangeAdd((LONG*)&v->counter, -(LONG)i);
    return v->counter;
}

/** subtract value from variable and test result.
 * Atomically subtracts i from v and returns true if the result is zero,
 * or false for all other cases.
 */
static inline unsigned int ua_atomic_sub_and_test(unsigned int i, ua_atomic_t *v)
{
    /* TODO: decrement and comparison is not atomic,
     * but in normal usecases as refcount there should nobody be left
     * to modify the refcnt when the final reference gets substracted.
     */
    InterlockedExchangeAdd((LONG*)&v->counter, -((LONG)i));
    return (v->counter == 0);
}

static inline unsigned int ua_atomic_dec_and_test(ua_atomic_t *v)
{
    return ua_atomic_sub_and_test(1, v);
}

static inline int ua_atomic_compare_and_swap(int *ptr, int oldval, int newval)
{
    return InterlockedCompareExchange((LONG*)ptr, newval, oldval);
}

static inline unsigned int ua_atomic_compare_and_swap_uint(unsigned int *ptr, unsigned int oldval, unsigned int newval)
{
    return InterlockedCompareExchange((LONG*)ptr, newval, oldval);
}

#endif /* end of include guard: UA_ATOMIC_H */

