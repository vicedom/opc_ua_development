/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
#ifndef UAPUBSUB_CALLBACKS_H
#define UAPUBSUB_CALLBACKS_H

#include <uapubsub_types.h>

#include <platform/platform.h>

#include <stdbool.h>
#include <stdint.h>

UA_BEGIN_EXTERN_C

/* Type for callback function pointers */
typedef void (*pubsub_cb)(void);

/* Types of callback functionpointers */
enum pubsub_cb_type {
    PUBSUB_CB_T_WGROUP_INIT,       ///< int (*)(void *arg);
    PUBSUB_CB_T_WGROUP_HDR,        ///< int (*)(struct ua_encoder_context *ctx, void *arg);
    PUBSUB_CB_T_WGROUP_PLHDR,      ///< int (*)(struct ua_encoder_context *ctx, void *arg);
    PUBSUB_CB_T_WGROUP_DATA,       ///< int (*)(struct ua_encoder_context *ctx, void *arg);
    PUBSUB_CB_T_WGROUP_CLEAR,      ///< int (*)(void *arg);
    PUBSUB_CB_T_WGROUP_PROCESSING, ///< bool (*)(void *arg);
    PUBSUB_CB_T_WRITER_INIT,       ///< int (*)(void *arg);
    PUBSUB_CB_T_WRITER_MSG,        ///< int (*)(struct ua_encoder_context *ctx, void *arg);
    PUBSUB_CB_T_WRITER_HDR,        ///< int (*)(struct ua_encoder_context *ctx, void *arg);
    PUBSUB_CB_T_WRITER_DATA, ///< int (*)(struct ua_encoder_context *ctx, void *arg, bool *valid, uint8_t *type, uint16_t* status);
    PUBSUB_CB_T_WRITER_CLEAR,        ///< int (*)(void *arg);
    PUBSUB_CB_T_READER_INIT,         ///< int (*)(void *arg);
    PUBSUB_CB_T_READER_CLEAR,        ///< int (*)(void *arg);
    PUBSUB_CB_T_READER_DATA,         ///< int (*)(struct ua_decoder_context *ctx, void *arg);
    PUBSUB_CB_T_READER_Q_DATACHANGE, ///< void (*)(void *arg, uint16_t status, const ua_datetime* timestamp);
    PUBSUB_CB_T_Q_ALLOC, ///< int (*)(struct pubsub_datavalue_samplingitem **queue, unsigned int *queue_size, void *arg);
    PUBSUB_CB_T_Q_FREE,  ///< int (*)(struct pubsub_datavalue_samplingitem *queue, void *arg);
    PUBSUB_CB_T_Q_INIT,  ///< int (*)(struct pubsub_datavalue_samplingitem *queue, unsigned int queue_size, void *arg);
    PUBSUB_CB_T_Q_CLEAR, ///< int (*)(struct pubsub_datavalue_samplingitem *queue, unsigned int queue_size, void *arg);
    PUBSUB_CB_T_Q_ANNOUNCE, ///< int (*)(struct pubsub_datavalue_samplingitem *queue, unsigned int queue_size, void *arg);
    PUBSUB_CB_T_Q_REVOKE,   ///< int (*)(struct pubsub_datavalue_samplingitem *queue, void *arg);
    PUBSUB_CB_T_Q_READ,     ///< int (*)(struct pubsub_datavalue_samplingitem *queue, void *arg);
    PUBSUB_CB_T_Q_WRITE,    ///< int (*)(struct pubsub_datavalue_samplingitem *queue, void *arg);
    PUBSUB_CB_T_MAX_ENUM_VALUE = INT32_MAX
};

/* Function pointer type of a dispatcher instance */
typedef int (*pubsub_dispatch_fn)(pubsub_handle obj, enum pubsub_cb_type type, pubsub_cb *old_cb, void **old_arg);

UA_END_EXTERN_C

#endif // UAPUBSUB_CALLBACKS_H
