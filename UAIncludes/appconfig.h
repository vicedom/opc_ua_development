/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef APPCONFIG_H_VSIHJ1RZ
#define APPCONFIG_H_VSIHJ1RZ

#include <stdint.h>
#include <stdbool.h>
#include <uabase/statuscode.h>
#include <uabase/structure/usertokentype.h>
#include <uaprotocol/uaseconv/seconv_types.h>

#define MAX_INDEX_ARRAY_LENGTH 15

UA_BEGIN_EXTERN_C

/* forward declaration */
struct sechan_config;

struct index_array {
    uint8_t indicies[MAX_INDEX_ARRAY_LENGTH];
    uint8_t num;
};

/**
 * UA Application Instance Certificate.
 * Identifies a certificate and key, does not contain the certificate
 * or key data.
 */
struct uaapp_certificate {
    const char *certificate;    /**< Certificate location specifier (file:// or store:// (only store for new certificates)) */
    const char *key;            /**< Key location specifier (file:// or store (identified by cert id)) */
    uint32_t    store;          /**< Store identifier */
    /* creation information - only required if certificate shall be generated automatically */
    uint32_t    days_valid;     /**< Number of days the created certificate shall be valid. */
    const char *algorithm;      /**< Signature algorithm used for signing the new certificate (sha1 or sha256) */
    uint32_t    key_length;     /**< Key length in bits of the new key pair. */
    uint32_t    issuer_index;   /**< Index specifying the issuer certificate (same as cert for self signed) */
    bool        create;         /**< true if certificate shall be generated if not found at above location */
};

struct uaserver_endpoint {
    const char *endpoint_url;             /**< endpoint url: e.g. "opc.tcp://hostname" */
    const char *bind_address;             /**< bind address: e.g. "0.0.0.0" for all IPs */
    uint32_t bind_port;                   /**< port number in host byte order */
    struct index_array security_policies; /**< Index array of security policies for this endpoint */
    struct index_array user_tokens;       /**< Index array of user token types for this endpoint */
};

struct uaserver_pkistore {
    const char *config;
    const char *import_dir;
};

struct uaserver_user_token {
    const char *name;
    enum ua_usertokentype type;
    enum ua_security_policy_id policy_id;
};

struct subscription_config {
    uint32_t num_sessions_with_subscriptions;      /* only this number of sessions can have a subscription */
    uint32_t num_publishrequests_per_session;      /* maximum queued publishrequests in one session */
    uint32_t num_subscriptions;                    /* total number of available subscriptions */
    uint32_t num_subscriptions_per_session;        /* maximum number of subscriptions in one session */
    uint32_t num_notificationmessages_per_session; /* aka retransmission queue, shall be at least twice as big as num_publishrequests_per_session */
    uint32_t max_notifications_per_publish;        /* sum of values and events in one publishresponse */
    uint32_t num_monitoreditems_per_subscription;  /* maximum number of monitoreditems in one subscription */
    uint32_t min_publishing_interval;              /* minimum supported publishing interval */
    uint32_t max_publishing_interval;              /* maximum supported publishing interval */
    uint32_t num_monitoreditems;                   /* total number of available monitored items */
    uint32_t min_sampling_interval;                /* minimum supported sampling interval */
    uint32_t max_sampling_interval;                /* maximum supported sampling interval */
    uint32_t max_monitoreditem_queue_size;         /* maximum supported number of values queued in one monitoreditem */
    uint32_t min_lifetime;                         /* minimum interval after which subscripiton times out (milliseconds) */
    uint32_t max_lifetime;                         /* maximum interval after which subscripiton times out (milliseconds) */
    uint32_t num_eventnotifiers;                   /* number of eventnotifiers and eventsources */
    uint32_t num_eventfields;                      /* number of eventfields */
};

struct session_config {
    uint32_t    num_sessions;                       /* max number of parallel sessions (per base) */
    uint32_t    num_calls;                          /* max number of parallel service calls (per base; not per session!; related to num_uatcpmsg_ctxts) */
    uint32_t    lifetime_check_interval;            /* time interval in ms at which session lifetime will be checked. */
    uint32_t    lifetime_min;                       /* minimum session lifetime in milliseconds */
    uint32_t    lifetime_max;                       /* maximum session lifetime in milliseconds */
    bool        keep_spare_session;                 /* When true, the oldest, not connected session is deleted if num_sessions is reached. */
    uint32_t    authentication_num_users;           /* max number of user supported for authentication */
    const char *authentication_passwd_file;         /* location of file to load users and passwords for authentication */
    uint32_t    authentication_store;               /* PKI store for x509 user authentication */
    uint32_t    authorization_num_users;            /* max number of user supported for authorization */
    const char *authorization_users_file;           /* location of file to load users for authorization */
    uint32_t    authorization_num_groups;           /* max number of groups supported for authorization */
    const char *authorization_groups_file;          /* location of file to load groups for authorization */
    uint32_t    max_references_per_node;            /* limits the maxium number of references returned in a browse request */
};

struct uatcpmsg_config {
    uint32_t num_conns;                             /* max number of parallel uatcp connections */
    uint32_t num_listeners;                         /* number of listener sockets, one for each endpoint */
    uint32_t num_ctxts;                             /* max number of parallel service calls (per base; not per connection!; related to num_session_calls) */
    uint32_t num_chunks;                            /* max number of chunks per message; multiply with UA_BUFFER_SIZE to get max message size. */
};

struct ipc_config {
    uint32_t ipc_size;                              /**< size of complete IPC memory segment. Created by ua_ipc_mem_create */
    uint32_t heap_size;                             /**< IPC heap size. Set to 0 to automatically assign all reamining memory of IPC mem */
    uint32_t queue_size;                            /**< Size of IPC queue. Number of messages that can be stored in one queue. */
    uint32_t num_queues;                            /**< number of IPC queues */
    uint32_t num_messages;                          /**< number of IPC messages */
    uint32_t num_services;                          /**< number of IPC services structs */
    bool outprocess_encoder;                        /**< run encoder in separate process */
    bool outprocess_seconv;                         /**< run seconv in separate process */
};

/** encoder/decoder configuration */
struct encoder_config {
    uint32_t max_call_depth;                        /**< maximum call depth for recursive structures */
    uint32_t max_string_length;                     /**< maximum string length */
    uint32_t max_bytestring_length;                 /**< maximum bytestring length */
    uint32_t max_array_length;                      /**< maximum array length */
};

struct network_config {
    uint32_t buffer_size;                           /**< size of network buffer. Must be at least 8196 byte to be compliant. */
    uint32_t num_buffers;                           /**< number of network buffers. */
    uint32_t num_buffers_per_connection;            /**< number of buffers per connection. */
    uint32_t max_connections_per_ip;                /**< maximum number of connection per IP address. */
};

struct seconv_config {
    uint32_t num_channels;                          /* max number of parallel secure channels (per base) */
    bool     keep_spare_channel;                    /* When true, the oldest channel without an activated session is deleted if num_channels is reached. */
    uint32_t min_security_token_lifetime;
    uint32_t max_security_token_lifetime;
};

struct endpoint_config {
    struct uaserver_endpoint *endpoints;            /**< array of endpoint objects */
    uint32_t num_endpoints;                         /**< number of elements in endpoints */
    struct sechan_config *sec_configs;              /**< array of security configurations (endpoints in terms of OPC UA) */
    uint32_t num_sec_config;                        /**< number of elements in sec_configs */
    struct uaapp_certificate *certificates;         /**< array of certificates, can be used for simple configuration */
    uint32_t num_certificates;                      /**< number of elements in certificates */
    struct sechan_cert *sechan_certs;               /**< array of securechannel certificates, can be used for advanced configuration */
    uint32_t num_sechan_certs;                      /**< number of elements in sechan_certs */
    struct uaserver_user_token *user_tokens;        /**< array of user tokens */
    uint32_t num_user_tokens;                       /**< number of elements in user_tokens */
    bool allow_deprecated_policies;                 /**< allow usage of deprecated security policies for the securechannel and user tokens */
};

struct pkistore_config {
        struct uaserver_pkistore *pkistores;
        uint32_t num_pkistores;
};

struct client_config {
    uint32_t num_clients;                           /**< maximum number of client objects, that can be allocated */
    uint32_t num_subscriptions;                     /**< maximum number of client subscription objects, that can be allocated */
    uint32_t store;                                 /**< pki store index to use for trust check */
    uint32_t certificate;                           /**< certificate index */
};

struct serverprofile {
    const char *uri;
};

struct server_config {
    uint32_t max_dimensions;                        /**< max number of dimensions by server address space model */
    uint32_t num_methods;                           /**< max number of methods that can be registerd at the call table */
    struct serverprofile *serverprofiles;           /**< array of supported serverprofiles */
    uint32_t num_serverprofiles;                    /**< number of elements in serverprofiles array */
};

/** application configuration structure */
struct appconfig {
    uint32_t xml_max_dom_nodes;                     /* max number of XML DOM nodes. Only used when compiled with XML support. */
    const char *manufacturer_name;                  /* Name of the server manufacturer, not loaded from settings.conf */
    const char *product_name;                       /* Name of the product, not loaded from settings.conf */
    const char *product_uri;                        /* URI of the product, not loaded from settings.conf */
    const char *software_version;                   /* software version of the product, not loaded from settings.conf */
    const char *build_number;                       /* build number of the product, not loaded from settings.conf */
    const char *application_name;                   /* Name of the application, not loaded from settings.conf */
    const char *application_uri;                    /* URI of the application, not loaded from settings.conf */
    const char *organization_unit;                  /* certificate: organization unit */
    const char *locality;                           /* certificate: locality (city) */
    const char *state;                              /* certificate: name of the state  */
    const char *country;                            /* certificate: two letter country code (DE, US, ...) */
    const char *ips;                                /* certificate: comma separated list of IP addresses */
    const char *dns;                                /* certificate: comma separated list of hostnames/DNS names */
    const char *email;                              /* certificate: comma separated list of email addresses */
    const char *domain_component;                   /* certificate: domain component */
    const char *ns0_filename;                       /* relative of absolute path to ns0.bin, if NULL the default 'ns0.bin' will be used */
    struct uatcpmsg_config uatcpmsg;
    struct seconv_config seconv;
    struct session_config session;
    struct endpoint_config endpoint;
    struct pkistore_config pkistore;
    uint32_t num_timers;
    struct ipc_config ipc;
    struct encoder_config encoder;
    struct network_config net;
    struct subscription_config subscription;
    struct server_config server;
    struct client_config client;
};

/** Global application configuration.
 * Compile in link this into your application.
 * This structure contains all runtime configuration options
 * used in all applicatons.
 * Libraries will use thes options without recompiling.
 */
extern struct appconfig g_appconfig;

UA_END_EXTERN_C

#endif /* end of include guard: APPCONFIG_H_VSIHJ1RZ */

