/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef FILTER_H_Y5NN6D8Q
#define FILTER_H_Y5NN6D8Q

struct ua_ipaddress;
struct ip_stat;

struct ua_ipfilter {
    struct ip_stat *ip_stat;
    unsigned int num_ip_stat;
    unsigned int max_ip_stat;
};

unsigned int ua_ipfilter_size(unsigned int num_sockets);
int ua_ipfilter_init(struct ua_ipfilter *ipfilter, void *data, unsigned int num_sockets);

int ua_ipfilter_add_ip(struct ua_ipfilter *ipfilter, struct ua_ipaddress *ip);
int ua_ipfilter_remove_ip(struct ua_ipfilter *ipfilter, struct ua_ipaddress *ip);

#endif /* end of include guard: FILTER_H_Y5NN6D8Q */

