/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
/****************************************************************************
** OPC UA Information Model Code generated from 'Opc.Ua.NodeSet2.xml'.
**
** Generated on: 2019-02-07
** Generated by: Unified Automation Perl Generator 1.0.0
**
** This code may soley be used with Unified Automation SDKs to create
** commercial OPC UA applications. It is not allowed to copy, distribute or
** modify the source code, nor any part of it.
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#ifndef _UAENCODER_PUBLISHEDDATASETDATATYPE_H_
#define _UAENCODER_PUBLISHEDDATASETDATATYPE_H_

UA_BEGIN_EXTERN_C

/**
 * @internal
 * @defgroup ua_encoder_publisheddatasetdatatype ua_publisheddatasetdatatype
 * @ingroup ua_encoder_structure
 * @{
 */

/* forward declarations */
struct ua_encoder_context;
struct ua_decoder_context;
struct ua_buffer;
struct ua_publisheddatasetdatatype;

int ua_encode_publisheddatasetdatatype(struct ua_encoder_context *ctx, const struct ua_publisheddatasetdatatype *val);
int ua_decode_publisheddatasetdatatype(struct ua_decoder_context *ctx, struct ua_publisheddatasetdatatype *val);

/** @}*/

UA_END_EXTERN_C

#endif /* _UAENCODER_PUBLISHEDDATASETDATATYPE_H_ */

