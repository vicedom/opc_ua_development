/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UABASE_BYTESTRING_H
#define UABASE_BYTESTRING_H

#include <uabase/base_config.h>
#include <platform/platform.h>
#include <stdint.h>
#include <stdlib.h> /* for size_t */
#include <stdbool.h> /* for bool */
#include <memory/memory_config.h>

UA_BEGIN_EXTERN_C

struct ua_string;

/** @ingroup ua_base
 * This Built-in DataType defines a value that is a sequence of Byte values.
 */
struct ua_bytestring {
    int32_t len; /**< Number of bytes, or -1 for a ‘null’ bytestring. */
    char *data;  /**< Pointer to actual array with bytes */
};

/**
 * Initializer for empty ua_bytestrings.
 * @relates ua_bytestring
 */
#define UA_BYTESTRING_INITIALIZER { 0, NULL }

void ua_bytestring_init(struct ua_bytestring *bs);
int  ua_bytestring_create(struct ua_bytestring *bs, size_t size);
void ua_bytestring_clear(struct ua_bytestring *bs);

size_t ua_bytestring_length(const struct ua_bytestring *bs);
char *ua_bytestring_data(struct ua_bytestring *bs);
const char *ua_bytestring_const_data(const struct ua_bytestring *bs);
bool ua_bytestring_is_null(const struct ua_bytestring *bs);

int ua_bytestring_compare(const struct ua_bytestring *a, const struct ua_bytestring *b) UA_PURE_FUNCTION;
int ua_bytestring_copy(struct ua_bytestring *dst, const struct ua_bytestring *src);

int ua_bytestring_set(struct ua_bytestring *bs, const char *src, int32_t len);
void ua_bytestring_attach(struct ua_bytestring *bs, char *src, int32_t len);
void ua_bytestring_set_null(struct ua_bytestring *bs);
int ua_bytestring_append(struct ua_bytestring *dst, const struct ua_bytestring *src);

#ifdef SUPPORT_FILE_IO
int ua_bytestring_from_file(struct ua_bytestring *bs, const char *path);
int ua_bytestring_to_file(struct ua_bytestring *bs, const char *path);
#endif

#ifdef ENABLE_TO_STRING
int ua_bytestring_to_string(const struct ua_bytestring *bs, struct ua_string *dst);
int ua_bytestring_snprintf(char *dst, size_t size, const struct ua_bytestring *bs);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_bytestring_from_string(struct ua_bytestring *bs, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

UA_END_EXTERN_C

#endif /* UABASE_BYTESTRING_H */

