/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_DIAGNOSTIC_INFO_H_
#define _UABASE_DIAGNOSTIC_INFO_H_

#include <platform/platform.h>
#include <uabase/base_config.h>

UA_BEGIN_EXTERN_C

/* these defines are also needed if diagnosticinfo is disabled */
#define UA_DIAGNOSITCINFO_SYMBOLIC_ID      0x01
#define UA_DIAGNOSITCINFO_NAMESPACE_URI    0x02
#define UA_DIAGNOSITCINFO_LOCALIZEDTEXT    0x04
#define UA_DIAGNOSITCINFO_LOCALE           0x08
#define UA_DIAGNOSITCINFO_ADDITIONAL_INFO  0x10
#define UA_DIAGNOSITCINFO_INNER_STATUSCODE 0x20
#define UA_DIAGNOSITCINFO_INNER_DIAG_INFO  0x40

#ifdef ENABLE_DIAGNOSTICS
#include <uabase/statuscode.h>
#include <uabase/string.h>

/* forward declarations */
struct ua_stringtable;
struct ua_localizedtext;

/** @ingroup ua_base
 * @struct ua_diagnosticinfo
 * Vendor-specific diagnostic information.
 *
 * The symbolic_id, namespace_uri, localized_text and locale fields are
 * indexes in a string table which is returned in the response
 * header. Only the index of the corresponding string in the string
 * table is encoded. An index of −1 indicates that there is no value
 * for the string.
 *
 * DiagnosticInfo allows unlimited nesting which could result in stack
 * overflow errors even if the message size is less than the maximum
 * allowed. Decoders shall support at least 100 nesting
 * levels. Decoders shall report an error if the number of nesting
 * levels exceeds what it supports.
 *
 * @var ua_diagnosticinfo::namespace_uri
 * The symbolic_id is defined within the context of a namespace.
 *
 * This namespace is represented as a string and is conveyed to the
 * Client in the @ref ua_responseheader::string_table "string_table"
 * parameter of the @ref ua_responseheader parameter. The
 * namespaceIndex parameter contains the index into the @ref
 * ua_responseheader::string_table "string_table" for this string. -1
 * indicates that no string is specified.
 *
 * The namespace_uri shall not be the standard OPC UA namespace. There
 * are no symbolic_ids provided for standard StatusCodes.
 *
 * @var ua_diagnosticinfo::symbolic_id
 * The symbolic_id shall be used to identify a vendor-specific error or
 * condition; typically the result of some server internal operation.
 *
 * The maximum length of this string is 32 characters. Servers wishing
 * to return a numeric return code should convert the return code into
 * a string and use this string as symbolic_id (e.g., "0xC0040007" or
 * "-4").
 *
 * This symbolic identifier string is conveyed to the Client in the
 * @ref ua_responseheader::string_table "string_table" parameter of
 * the @ref ua_responseheader parameter. The symbolic_id parameter
 * contains the index into the @ref ua_responseheader::string_table
 * "string_table" for this string. -1 indicates that no string is
 * specified.
 *
 * The symbolic_id shall not contain StatusCodes. If the localized_text
 * contains a translation for the description of a StatusCode, the
 * symbolic_id is -1.
 *
 * @var ua_diagnosticinfo::locale
 * The locale part of the vendor-specific localized text describing
 * the symbolic id.
 *
 * This localized text string is conveyed to the Client in the @ref
 * ua_responseheader::string_table "string_table" parameter of the
 * @ref ua_responseheader parameter. The locale parameter contains
 * the index into the @ref ua_responseheader::string_table
 * "string_table" for this string. -1 indicates that no string is
 * specified.
 *
 * @var ua_diagnosticinfo::localized_text
 * A vendor-specific localized text string that describes the symbolic id.
 *
 * The maximum length of this text string is 256 characters. This
 * localized text string is conveyed to the Client in the @ref
 * ua_responseheader::string_table "string_table" parameter of the
 * @ref ua_responseheader parameter. The localized_text parameter
 * contains the index into the @ref ua_responseheader::string_table
 * "string_table" for this string. -1 indicates that no string is
 * specified.
 *
 * The localized_text refers to the symbolic_id if present or the string
 * that describes the standard StatusCode if the server provides
 * translations. If the index is -1, the server has no translation to
 * return and the client should use the invariant StatusCode
 * description from the specification.
 *
 * @var ua_diagnosticinfo::additional_info
 * Vendor-specific diagnostic information.
 *
 * @var ua_diagnosticinfo::statuscode
 * A status code provided by an underlying system.
 *
 * Many applications will make calls into underlying systems during
 * OPC UA request processing. An OPC UA Server has the option of
 * reporting the status from the underlying system in the diagnostic
 * info.
 *
 * @var ua_diagnosticinfo::inner_diag_info
 * The diagnostic info associated with the inner statuscode.
 */
struct ua_diagnosticinfo {
    uint8_t encoding;
    int32_t symbolic_id;
    int32_t namespace_uri;
    int32_t localized_text;
    int32_t locale;
    struct ua_string additional_info;
    ua_statuscode statuscode;
    struct ua_diagnosticinfo *inner_diag_info;
    struct ua_stringtable *string_table;
};

void ua_diagnosticinfo_init(struct ua_diagnosticinfo *h, struct ua_stringtable *string_table);
void ua_diagnosticinfo_clear(struct ua_diagnosticinfo *h);

int ua_diagnosticinfo_compare(const struct ua_diagnosticinfo *a, const struct ua_diagnosticinfo *b);
int ua_diagnosticinfo_copy(struct ua_diagnosticinfo *dst, const struct ua_diagnosticinfo *src);

int ua_diagnosticinfo_set_namespaceuri(struct ua_diagnosticinfo *info, const struct ua_string *namespaceuri);
int ua_diagnosticinfo_get_namespaceuri(struct ua_diagnosticinfo *info, struct ua_string *namespaceuri);

int ua_diagnosticinfo_set_symbolicid(struct ua_diagnosticinfo *info, const struct ua_string *symbolicid);
int ua_diagnosticinfo_get_symbolicid(struct ua_diagnosticinfo *info, struct ua_string *symbolicid);

int ua_diagnosticinfo_set_localizedtext(struct ua_diagnosticinfo *info, const struct ua_localizedtext *lt);
int ua_diagnosticinfo_get_localizedtext(struct ua_diagnosticinfo *info, struct ua_localizedtext *lt);

int ua_diagnosticinfo_set_additionalinfo(struct ua_diagnosticinfo *info, const struct ua_string *additionalinfo);
int ua_diagnosticinfo_get_additionalinfo(struct ua_diagnosticinfo *info, struct ua_string *additionalinfo);

int ua_diagnosticinfo_set_statuscode(struct ua_diagnosticinfo *info, const ua_statuscode *statuscode);
int ua_diagnosticinfo_get_statuscode(struct ua_diagnosticinfo *info, ua_statuscode *statuscode);

int ua_diagnosticinfo_set_innerdiagnosticinfo(struct ua_diagnosticinfo *info, const struct ua_diagnosticinfo *inner_info);
int ua_diagnosticinfo_get_innerdiagnosticinfo(struct ua_diagnosticinfo *info, struct ua_diagnosticinfo *inner_info);

#else

#define ua_diagnosticinfo_copy(x, y) 0
#define ua_diagnosticinfo_clear(x)

#endif /* ENABLE_DIAGNOSTICS */

UA_END_EXTERN_C

#endif /* _UABASE_DIAGNOSTIC_INFO_H_ */
