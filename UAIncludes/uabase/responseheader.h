/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_RESPONSEHEADER_H_
#define _UABASE_RESPONSEHEADER_H_

#include <uabase/datetime.h>
#include <uabase/statuscode.h>
#include <uabase/string.h>

UA_BEGIN_EXTERN_C

#ifdef ENABLE_DIAGNOSTICS
#include <uabase/diagnosticinfo.h>
#include <uabase/stringtable.h>
#endif /* ENABLE_DIAGNOSTICS */

/**
 * @ingroup ua_base
 * @brief Common parameters for all responses.
 */
struct ua_responseheader {
    /** The time the Server sent the response */
    ua_datetime timestamp;
    /** The handle given by the Client to the request */
    uint32_t request_handle;
    /** OPC UA defined result of the Service invocation. */
    ua_statuscode service_result;
#ifdef ENABLE_DIAGNOSTICS
    /** Diagnostic information for the Service invocation.
     *
     * This parameter is empty if diagnostics information was not
     * requested in the request header. */
    struct ua_diagnosticinfo diag_info;
    /** There is one string in this list for each unique namespace,
     * symbolic identifier, and localized text string contained in all
     * of the diagnostics information parameters contained in the
     * response DiagnosticInfo.
     *
     * Each is identified within this table by its zero-based index. */
    struct ua_stringtable string_table;
#endif
    /* additionalHeader: reserverd for future use */
};

void ua_responseheader_init(struct ua_responseheader *h);
void ua_responseheader_clear(struct ua_responseheader *h);

UA_END_EXTERN_C

#endif /* _UABASE_RESPONSEHEADER_H_ */
