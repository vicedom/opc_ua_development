/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UABASE_STRING_H
#define UABASE_STRING_H

#include <uabase/base_config.h>
#include <stdint.h>
#include <stdbool.h>
#include <platform/platform.h>

UA_BEGIN_EXTERN_C

/** @ingroup ua_base
 * This encapsulates a UTF-8 encoded OPC UA string.
 * Note, on the wire OPC UA String are not zero terminated,
 * in memory we always add a zero terminator to avoid problems
 * with C functions that expect zero terminated strings.
 */
struct ua_string {
    uint32_t len : 31;     /**< Length of the string. */
    uint32_t free: 1;      /**< Bit to indicate if the data pointer must be freed. */
    union { /* unfortunately anonymous unions are only part of the C11 standard */
        const char *cdata; /**< Const pointer to the data. */
        char *data;        /**< Non-const pointer to the data. */
    } d;                   /**< The string data. */
};

/**
 * Initializer for null strings.
 * @relates ua_string
 */
#define UA_STRING_INITIALIZER { 0, 0, { NULL } }
/**
 * Initializer for ua_strings from string constants.
 * @relates ua_string
 */
#define UA_STRING_FROM_C_STRING(s) { sizeof(s)-1, 0, { s } }

/** Format specifier for printing ua_strings.
 * @code
 * void print_displayname(const ua_string *s)
 * {
 *     printf("displayname: %" UA_STRING_FMT "\n", UA_STRING_ARGS(s));
 * }
 * @endcode
 */
#define UA_STRING_FMT ".*s"
/** Format arguments for UA_STRING_FMT.
 * @code
 * void print_displayname(const ua_string *s)
 * {
 *     printf("displayname: %" UA_STRING_FMT "\n", UA_STRING_ARGS(s));
 * }
 * @endcode
 */
#define UA_STRING_ARGS(s) (int)ua_string_length(s), ua_string_const_data(s)
/** Used for trace usernames using TRACE_USERNAME. */
#define UA_STRING_USERNAME_ARGS(s) (int)ua_string_length(s), TRACE_USERNAME(ua_string_const_data(s))
/** Used for trace passwords using TRACE_PASSWORD. */
#define UA_STRING_PASSWORD_ARGS(s) (int)ua_string_length(s), TRACE_PASSWORD(ua_string_const_data(s))

void ua_string_init(struct ua_string *s);
void ua_string_clear(struct ua_string *s);

int ua_string_allocate(struct ua_string *s, size_t size);

int ua_string_set(struct ua_string *s, const char *src);
int ua_string_setn(struct ua_string *s, const char *src, size_t len);

void ua_string_attach(struct ua_string *s, char *src);
void ua_string_attachn(struct ua_string *s, char *src, size_t len);

void ua_string_attach_const(struct ua_string *s, const char *src);
void ua_string_attachn_const(struct ua_string *s, const char *src, size_t len);

int ua_string_smart_attach_const(struct ua_string *s, const char *src);
int ua_string_smart_attachn_const(struct ua_string *s, const char *src, size_t len);

int ua_string_compare_lex(const struct ua_string *a, const struct ua_string *b) UA_PURE_FUNCTION;
int ua_string_compare(const struct ua_string *a, const struct ua_string *b) UA_PURE_FUNCTION;
int ua_string_compare_const(const struct ua_string *a, const char *b) UA_PURE_FUNCTION;
int ua_string_comparen_const(const struct ua_string *a, const char *b, size_t len);
int ua_string_copy(struct ua_string *dst, const struct ua_string *src);
int ua_string_cat(struct ua_string *dst, size_t size, const struct ua_string *src);
int ua_string_catf(struct ua_string *dst, size_t size, const char *fmt, ...) UA_FORMAT_ARGUMENT(3, 4);
int ua_string_snprintf(struct ua_string *s, size_t size, const char *fmt, ...) UA_FORMAT_ARGUMENT(3, 4);

size_t ua_string_length(const struct ua_string *s);
char *ua_string_data(struct ua_string *s);
const char *ua_string_const_data(const struct ua_string *s);
bool ua_string_is_null(const struct ua_string *s);
bool ua_string_is_empty(const struct ua_string *s);

int ua_string_index_of(const struct ua_string *s, const char *substr);
int ua_string_replace(struct ua_string *s, const char *search, const char *replace);
int ua_string_trim(struct ua_string *s);

#ifdef SUPPORT_FILE_IO
int ua_string_from_file(struct ua_string *s, const char *path);
int ua_string_to_file(struct ua_string *s, const char *path);
#endif /* SUPPORT_FILE_IO */

UA_END_EXTERN_C

#endif /* UABASE_STRING_H */
