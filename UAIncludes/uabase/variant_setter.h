/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_VARIANT_SETTER_H_
#define _UABASE_VARIANT_SETTER_H_

#include <platform/platform.h>
#include "variant_types.h"

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_variant_setter ua_variant_setter
 * @brief Setter and attach functions for the @ref ua_base_variant structure.
 *
 * Setter functions set a copy of the value as the new value of the structure. Thus
 * the original value must still be cleared.
 *
 * Attach functions attach the original value as the new value of the structure. Thus
 * the original value must NOT be cleared.
 *
 * In both cases the original variant is not cleared, so if there is data contained
 * in the variant @ref ua_variant_clear must be called before calling any of these functions.
 *
 * @ingroup ua_base_variant
 * @{
 */

/* scalar set */
void ua_variant_set_bool(struct ua_variant *v, bool val);
void ua_variant_set_byte(struct ua_variant *v, uint8_t val);
void ua_variant_set_sbyte(struct ua_variant *v, int8_t val);
void ua_variant_set_uint16(struct ua_variant *v, uint16_t val);
void ua_variant_set_int16(struct ua_variant *v, int16_t val);
void ua_variant_set_uint32(struct ua_variant *v, uint32_t val);
void ua_variant_set_int32(struct ua_variant *v, int32_t val);
void ua_variant_set_uint64(struct ua_variant *v, uint64_t val);
void ua_variant_set_int64(struct ua_variant *v, int64_t val);
void ua_variant_set_float(struct ua_variant *v, float val);
void ua_variant_set_double(struct ua_variant *v, double val);
void ua_variant_set_datetime(struct ua_variant *v, ua_datetime val);
int ua_variant_set_string(struct ua_variant *v, const char *val);
int ua_variant_set_stringn(struct ua_variant *v, const char *val, size_t len);
int ua_variant_set_bytestring(struct ua_variant *v, const char *data, int len);
int ua_variant_set_xmlelement(struct ua_variant *v, const char *data, int len);
int ua_variant_set_nodeid(struct ua_variant *v, const struct ua_nodeid *id);
int ua_variant_set_expandednodeid(struct ua_variant *v, const struct ua_expandednodeid *id);
int ua_variant_set_guid(struct ua_variant *v, const struct ua_guid *guid);
void ua_variant_set_statuscode(struct ua_variant *v, ua_statuscode status);
int ua_variant_set_qualifiedname(struct ua_variant *v, int nsindex, const char *name);
int ua_variant_set_localizedtext(struct ua_variant *v, const char *locale, const char *text);
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
int ua_variant_set_datavalue(struct ua_variant *v, const struct ua_datavalue *value);
#endif
int ua_variant_set_extensionobject(struct ua_variant *v, const void *obj, const struct ua_nodeid *type_id);
int ua_variant_set_extensionobject_binary(struct ua_variant *v, const struct ua_bytestring *data, const struct ua_nodeid *encoding_id);

/* scalar attach */
void ua_variant_attach_string(struct ua_variant *v, char *val);
void ua_variant_attach_stringn(struct ua_variant *v, char *val, size_t len);
int ua_variant_smart_attach_const_string(struct ua_variant *v, const char *val);
int ua_variant_smart_attach_const_stringn(struct ua_variant *v, const char *val, size_t len);
int ua_variant_attach_scalar(struct ua_variant *v, enum ua_variant_type type, void *val);
int ua_variant_attach_extensionobject(struct ua_variant *v, void *obj, const struct ua_nodeid *type_id);

/* array set */
int ua_variant_set_bool_array(struct ua_variant *v, bool *val, size_t num);
int ua_variant_set_byte_array(struct ua_variant *v, uint8_t *val, size_t num);
int ua_variant_set_sbyte_array(struct ua_variant *v, int8_t *val, size_t num);
int ua_variant_set_uint16_array(struct ua_variant *v, uint16_t *val, size_t num);
int ua_variant_set_int16_array(struct ua_variant *v, int16_t *val, size_t num);
int ua_variant_set_uint32_array(struct ua_variant *v, uint32_t *val, size_t num);
int ua_variant_set_int32_array(struct ua_variant *v, int32_t *val, size_t num);
int ua_variant_set_uint64_array(struct ua_variant *v, uint64_t *val, size_t num);
int ua_variant_set_int64_array(struct ua_variant *v, int64_t *val, size_t num);
int ua_variant_set_float_array(struct ua_variant *v, float *val, size_t num);
int ua_variant_set_double_array(struct ua_variant *v, double *val, size_t num);
int ua_variant_set_extensionobject_array(struct ua_variant *v, const void *obj, size_t num, const struct ua_nodeid *type_id);
int ua_variant_set_array(struct ua_variant *v, enum ua_variant_type type, const void *values, size_t num);
int ua_variant_set_string_array(struct ua_variant *v, const char **val, size_t num);

/* array attach */
int ua_variant_attach_array(struct ua_variant *v, enum ua_variant_type type, void *val, size_t num);
int ua_variant_smart_attach_string_array(struct ua_variant *v, const char **val, size_t num);

/* matrix set */
int ua_variant_set_matrix(struct ua_variant *v, enum ua_variant_type type, const void *values, size_t num, const int32_t *dimensions, int32_t num_dimensions);

/** @} */

UA_END_EXTERN_C

#endif /* inlcude guard */
