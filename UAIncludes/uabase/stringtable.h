/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_STRINGTABLE_H_
#define _UABASE_STRINGTABLE_H_

#include <platform/platform.h>
#include <uabase/base_config.h>

UA_BEGIN_EXTERN_C

/* forward declarations */
struct ua_string;
struct util_map;

/** @ingroup ua_base */
struct ua_stringtable {
    struct util_map *map;
    struct ua_string *table;
    int table_num_elements;
};

int ua_stringtable_init(struct ua_stringtable *st, int num_entries);
void ua_stringtable_clear(struct ua_stringtable *st);

int ua_stringtable_add_string(struct ua_stringtable *st, const struct ua_string *string, int *index);
int ua_stringtable_get_string(struct ua_stringtable *st, struct ua_string *string, int index);

UA_END_EXTERN_C

#endif /* _UABASE_STRINGTABLE_H_ */
