/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_DICT_H_
#define _UABASE_DICT_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include "base.h"
#include "type_identifier.h"
#include "extensionobject.h"

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_dict ua_dict
 * @ingroup ua_base
 *
 * Provides structs to describe UA datatypes and implements base
 * functions based on these structs.
 *
 * @{
 */

/* The values of the UA_DICT_FLAG_* defines are carefully
 * chosen and important for functioning of below functions.
 * DO NOT CHANGE THESE VALUES!
 */
/** None of the other flags is set */
#define UA_DICT_FLAG_DEFAULT       0x00
/** Flag to set for namespace zero fields */
#define UA_DICT_FLAG_NS0           0x01
/** Flag to set for array fields */
#define UA_DICT_FLAG_ARRAY         0x02
/** Flag to set for omitted fields */
#define UA_DICT_FLAG_OMITTED       0x04
/** Flag to set for fixed size fields */
#define UA_DICT_FLAG_FIXED_SIZE    0x08
/** Flag to set for optional fields in structures with optional fields */
#define UA_DICT_FLAG_OPTIONAL      0x10
/** Flag to set for the union type field in unions, must be the first field */
#define UA_DICT_FLAG_UNION_TYPE    0x20
/** Flag to set for the encoding mask field in structures with optional fields, must be the first field */
#define UA_DICT_FLAG_ENCODING_MASK 0x40

#define UA_DICT_ENABLE_NAME

#ifdef UA_DICT_ENABLE_NAME
# define UA_DICT_NAME(name) name,
#else
/** Define to handle the name field inside @ref ua_dict_field and @ref ua_dict_structure depending on the UA_DICT_ENABLE_NAME define */
# define UA_DICT_NAME(name)
#endif

#ifdef ENABLE_DIAGNOSTICS
/** Flag for diagnostic info fields, handles the omitted field flag depending on the ENABLE_DIAGNOSTICS define */
# define UA_DICT_FLAG_DIAGNOSITICINFO UA_DICT_FLAG_DEFAULT
#else
# define UA_DICT_FLAG_DIAGNOSITICINFO UA_DICT_FLAG_OMITTED
#endif

/** Type of the type field, determines maximum number of types */
typedef uint16_t ua_dict_type_t;
/** Type of size related fields, determines maximum size of structures */
typedef uint16_t  ua_dict_size_t;
/** Type of the ns field, determines maximum number and value of namespaces */
typedef uint8_t  ua_dict_ns_t;

/**
 * Struct to describe each field of a structure.
 */
struct ua_dict_field {
#ifdef UA_DICT_ENABLE_NAME
    const char *name;      /**< name of the field as used in XML or json encoding */
#endif
    ua_dict_type_t type;   /**< local type of the field */
    uint8_t        flags;  /**< bitmask with flags for this field */
    ua_dict_ns_t   ns;     /**< namespace index of the type */
    ua_dict_size_t offset; /**< offset of the field from the start of the structure in bytes */
};

/**
 * Struct to describe another structure.
 */
struct ua_dict_structure {
#ifdef UA_DICT_ENABLE_NAME
    const char *name;                   /**< name of the structure as used in XML or json encoding */
#endif
    const struct ua_dict_field *fields; /**< array of field descriptions */
    uint32_t binary_encoding_id;        /**< encoding id used by the ua binary encoding, only numerics are possible */
    uint32_t type_id;                   /**< type id, only numerics are possible */
    ua_dict_size_t num_fields;          /**< number of elements in fields */
    ua_dict_size_t size;                /**< Size of the described struct in bytes */
};

void ua_dict_clear_structure(const struct ua_dict_structure *strct, void *val);
void ua_dict_free_structure(const struct ua_dict_structure *strct, void *val);
int ua_dict_compare_structure(const struct ua_dict_structure *strct, const void *a, const void *b);
int ua_dict_copy_structure(const struct ua_dict_structure *strct, void *dst, const void *src);

int ua_dict_get_scalar(const struct ua_dict_structure *strct, void *val, int field_idx, void **scalar);
int ua_dict_get_array(const struct ua_dict_structure *strct, void *val, int field_idx, void **array, int32_t *array_len);
int ua_dict_resize_array(const struct ua_dict_structure *strct, void *val, int field_idx, int32_t new_len);
int ua_dict_get_field_info(const struct ua_dict_structure *strct, int field_idx, uint32_t *ua_dict_flags, struct ua_nodeid *type_id, struct ua_nodeid *binary_encoding_id);

#ifdef UA_DICT_ENABLE_NAME
int ua_dict_field_name_to_idx(const struct ua_dict_structure *strct, const char *field_name);

static inline int ua_dict_get_scalar_by_name(const struct ua_dict_structure *strct, void *val, const char *field_name, void **scalar)
{
    return ua_dict_get_scalar(strct, val, ua_dict_field_name_to_idx(strct, field_name), scalar);
}

static inline int ua_dict_get_array_by_name(const struct ua_dict_structure *strct, void *val, const char *field_name, void **array, int32_t *array_len)
{
    return ua_dict_get_array(strct, val, ua_dict_field_name_to_idx(strct, field_name), array, array_len);
}

static inline int ua_dict_resize_array_by_name(const struct ua_dict_structure *strct, void *val, const char *field_name, int32_t new_len)
{
    return ua_dict_resize_array(strct, val, ua_dict_field_name_to_idx(strct, field_name), new_len);
}

static inline int ua_dict_get_field_info_by_name(const struct ua_dict_structure *strct, const char *field_name, uint32_t *ua_dict_flags, struct ua_nodeid *type_id, struct ua_nodeid *binary_encoding_id)
{
    return ua_dict_get_field_info(strct, ua_dict_field_name_to_idx(strct, field_name), ua_dict_flags, type_id, binary_encoding_id);
}
#endif

/** @} */

UA_END_EXTERN_C

#endif /* _UABASE_DICT_H_*/

