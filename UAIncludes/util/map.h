/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UTIL_MAP_H_
#define _UTIL_MAP_H_

#include <platform/platform.h>

/**
 * @defgroup util_map_group map
 * @ingroup util
 *
 * Provides a dictionary which is based on a sorted list.
 * Lookup is implemented using a binary search and operates in O(log n).
 * Insert and remove operations have a complexity of O(n).
 * This implementation should be used for mid-sized amount of data (100-10000 elements),
 * where a list would be too slow. For bigger data the util_hash_map should be used.
 * The advantage of util_map over util_hash_map is, that util_map applies
 * an order on the elements. When iterating over util_map the elements are sorted
 * by key, whereas with util_hash_map the elements are arbitrarily ordered.
 *
 * @{
 */

/** Identifier for an invalid index */
#define UTIL_MAP_NIL -1

/** Foreach loop for iterating over a map */
#define util_map_foreach(i, map) for (i = util_map_first(map); i != UTIL_MAP_NIL; i = util_map_next(map, i))

UA_BEGIN_EXTERN_C

/**
 * Compare function for keys stored in the map.
 * Must return zero if elements are equal and negative/positive
 * value if one element is smaller/bigger than the other.
 */
typedef int (*util_map_compar)(const void *a, const void *b);
/**
 * Delete function for the keys/values store in the map.
 * Must delete all resources connected to the key/value.
 */
typedef void (*util_map_delete)(void *key, void *value);

/**
 * Structure for a map, see also @ref util_map_group.
 * @see util_map_group
 */
struct util_map {
    void *nodes;           /**< preallocted array of map elements */
    util_map_compar comp;  /**< comparision function for sorting map elements */
    int max_count;         /**< maximum number of map elements (length of nodes) */
    int count;             /**< current number of elements in map */
};

/** Iterator for the map */
typedef int util_map_iterator;

int util_map_size(int num_elements);
void util_map_init(struct util_map *m, util_map_compar comp, void *data, int size);
void *util_map_clear(struct util_map *m);
int util_map_count(const struct util_map *m);
int util_map_maxcount(const struct util_map *m);
int util_map_insert(struct util_map *m, void *key, void *newval, void **oldval);
int util_map_replace(struct util_map *m, void *key, void *newval, void **oldval);
void *util_map_remove(struct util_map *m, const void *key);
void *util_map_lookup(const struct util_map *m, const void *key);
util_map_iterator util_map_find(const struct util_map *m, const void *key);
util_map_iterator util_map_first(const struct util_map *m);
util_map_iterator util_map_next(const struct util_map *m, util_map_iterator it);
void *util_map_value(const struct util_map *m, util_map_iterator it);
void *util_map_key(const struct util_map *m, util_map_iterator it);
void util_map_delete_all(struct util_map *m, util_map_delete del);

UA_END_EXTERN_C

/** @} */

#endif /* end of include guard: _UTIL_MAP_H_ */
