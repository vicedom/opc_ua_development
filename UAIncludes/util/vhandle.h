/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef __VHANDLE_H__
#define __VHANDLE_H__

/**
 * @struct versioned_handle
 * @ingroup util
 *
 * Provides the data structure for versioned handles.
 * This is a structure which can be used to provide handles which can be used
 * for index based access of data (e.g. array) to a caller. This handles are
 * opaque for the caller as an int value.
 * Internally you can use this struct to access the offset and id part of the
 * handle. The id can be used as a version number. If e.g. the caller uses a
 * handle with a id part different from the version stored in the data container
 * this handle is outdated.
 * The offset can be used to apply common range checks and to acces the correct
 * data in an array.
 */
union util_versioned_handle {
    unsigned int value;           /**< public handle value */
    struct {
        unsigned int offset : 8;  /**< offset in array for lookup */
        unsigned int id     : 24; /**< unique id for versioning the handle */
    } v;
};

#endif /* end of include guard: __VHANDLE_H__ */

