/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UTIL_URL_H
#define UTIL_URL_H

#include <stdint.h>
#include <stdlib.h>
#include <platform/platform.h>

/**
 * @defgroup util_url url
 * @ingroup util
 * @brief Url utility functions.
 * @{
 */

/**
 * @brief Struct to represent the elements of a URL.
 *
 * The strings are not zero terminated, so the associated length field
 * must be respected.
 */
struct util_url {
    const char *scheme;   /**< scheme or protocol, like http or opc.tcp */
    size_t scheme_len;    /**< length of the scheme */
    const char *host;     /**< hostname or ip address */
    size_t host_len;      /**< length of the host */
    const char *path;     /**< trailing data after the port */
    size_t path_len;      /**< length of the path */
    uint16_t port;        /**< port number */
};

UA_BEGIN_EXTERN_C

int util_url_parse(const char *string, struct util_url *elements);

UA_END_EXTERN_C

/** @} */

#endif /* UTIL_URL_H */
