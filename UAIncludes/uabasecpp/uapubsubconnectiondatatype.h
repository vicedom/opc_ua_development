/******************************************************************************
** uapubsubconnectiondatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaPubSubConnectionDataType class.
**
******************************************************************************/
#ifndef UAPUBSUBCONNECTIONDATATYPE_H
#define UAPUBSUBCONNECTIONDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uaextensionobject.h"
#include "uakeyvaluepair.h"
#include "uareadergroupdatatype.h"
#include "uastring.h"
#include "uavariant.h"
#include "uawritergroupdatatype.h"
#include "uaarraytemplates.h"

class UaDataValue;

class UABASE_EXPORT UaPubSubConnectionDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_PubSubConnectionDataType.
 *
 *  This class encapsulates the native OpcUa_PubSubConnectionDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaPubSubConnectionDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared PubSubConnectionDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaPubSubConnectionDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaPubSubConnectionDataType
{
    UA_DECLARE_PRIVATE(UaPubSubConnectionDataType)
public:
    UaPubSubConnectionDataType();
    UaPubSubConnectionDataType(const UaPubSubConnectionDataType &other);
    UaPubSubConnectionDataType(const OpcUa_PubSubConnectionDataType &other);
    UaPubSubConnectionDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        const UaVariant&publisherId,
        const UaString& transportProfileUri,
        UaExtensionObject& address,
        const UaKeyValuePairs &connectionProperties,
        UaExtensionObject& transportSettings,
        const UaWriterGroupDataTypes &writerGroups,
        const UaReaderGroupDataTypes &readerGroups
        );
    UaPubSubConnectionDataType(const UaExtensionObject &extensionObject);
    UaPubSubConnectionDataType(const OpcUa_ExtensionObject &extensionObject);
    UaPubSubConnectionDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaPubSubConnectionDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaPubSubConnectionDataType();

    void clear();

    bool operator==(const UaPubSubConnectionDataType &other) const;
    bool operator!=(const UaPubSubConnectionDataType &other) const;

    UaPubSubConnectionDataType& operator=(const UaPubSubConnectionDataType &other);

    OpcUa_PubSubConnectionDataType* copy() const;
    void copyTo(OpcUa_PubSubConnectionDataType *pDst) const;

    static OpcUa_PubSubConnectionDataType* clone(const OpcUa_PubSubConnectionDataType& source);
    static void cloneTo(const OpcUa_PubSubConnectionDataType& source, OpcUa_PubSubConnectionDataType& copy);

    void attach(OpcUa_PubSubConnectionDataType *pValue);
    OpcUa_PubSubConnectionDataType* detach(OpcUa_PubSubConnectionDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setPubSubConnectionDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setPubSubConnectionDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setPubSubConnectionDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPubSubConnectionDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setPubSubConnectionDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        const UaVariant&publisherId,
        const UaString& transportProfileUri,
        UaExtensionObject& address,
        const UaKeyValuePairs &connectionProperties,
        UaExtensionObject& transportSettings,
        const UaWriterGroupDataTypes &writerGroups,
        const UaReaderGroupDataTypes &readerGroups
        );

    UaString getName() const;
    OpcUa_Boolean getEnabled() const;
    UaVariant getPublisherId() const;
    UaString getTransportProfileUri() const;
    UaExtensionObject getAddress() const;
    void getConnectionProperties(UaKeyValuePairs& connectionProperties) const;
    UaExtensionObject getTransportSettings() const;
    void getWriterGroups(UaWriterGroupDataTypes& writerGroups) const;
    void getReaderGroups(UaReaderGroupDataTypes& readerGroups) const;

    void setName(const UaString& name);
    void setEnabled(OpcUa_Boolean enabled);
    void setPublisherId(const UaVariant&publisherId);
    void setTransportProfileUri(const UaString& transportProfileUri);
    void setAddress(UaExtensionObject& address);
    void setConnectionProperties(const UaKeyValuePairs &connectionProperties);
    void setTransportSettings(UaExtensionObject& transportSettings);
    void setWriterGroups(const UaWriterGroupDataTypes &writerGroups);
    void setReaderGroups(const UaReaderGroupDataTypes &readerGroups);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_PubSubConnectionDataType.
 *
 *  This class encapsulates an array of the native OpcUa_PubSubConnectionDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaPubSubConnectionDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaPubSubConnectionDataTypes
{
public:
    UaPubSubConnectionDataTypes();
    UaPubSubConnectionDataTypes(const UaPubSubConnectionDataTypes &other);
    UaPubSubConnectionDataTypes(OpcUa_Int32 length, OpcUa_PubSubConnectionDataType* data);
    virtual ~UaPubSubConnectionDataTypes();

    UaPubSubConnectionDataTypes& operator=(const UaPubSubConnectionDataTypes &other);
    const OpcUa_PubSubConnectionDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_PubSubConnectionDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaPubSubConnectionDataTypes &other) const;
    bool operator!=(const UaPubSubConnectionDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_PubSubConnectionDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_PubSubConnectionDataType* data);
    OpcUa_PubSubConnectionDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_PubSubConnectionDataType* rawData() const {return m_data;}
    inline OpcUa_PubSubConnectionDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setPubSubConnectionDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setPubSubConnectionDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setPubSubConnectionDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPubSubConnectionDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPubSubConnectionDataTypes(OpcUa_Int32 length, OpcUa_PubSubConnectionDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_PubSubConnectionDataType* m_data;
};

#endif // UAPUBSUBCONNECTIONDATATYPE_H

