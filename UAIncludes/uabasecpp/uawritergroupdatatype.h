/******************************************************************************
** uawritergroupdatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaWriterGroupDataType class.
**
******************************************************************************/
#ifndef UAWRITERGROUPDATATYPE_H
#define UAWRITERGROUPDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uadatasetwriterdatatype.h"
#include "uaendpointdescription.h"
#include "uaextensionobject.h"
#include "uakeyvaluepair.h"
#include "uastring.h"
#include "uaarraytemplates.h"

class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaWriterGroupDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_WriterGroupDataType.
 *
 *  This class encapsulates the native OpcUa_WriterGroupDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaWriterGroupDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared WriterGroupDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaWriterGroupDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaWriterGroupDataType
{
    UA_DECLARE_PRIVATE(UaWriterGroupDataType)
public:
    UaWriterGroupDataType();
    UaWriterGroupDataType(const UaWriterGroupDataType &other);
    UaWriterGroupDataType(const OpcUa_WriterGroupDataType &other);
    UaWriterGroupDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        OpcUa_MessageSecurityMode securityMode,
        const UaString& securityGroupId,
        const UaEndpointDescriptions &securityKeyServices,
        OpcUa_UInt32 maxNetworkMessageSize,
        const UaKeyValuePairs &groupProperties,
        OpcUa_UInt16 writerGroupId,
        OpcUa_Double publishingInterval,
        OpcUa_Double keepAliveTime,
        OpcUa_Byte priority,
        const UaStringArray &localeIds,
        const UaString& headerLayoutUri,
        UaExtensionObject& transportSettings,
        UaExtensionObject& messageSettings,
        const UaDataSetWriterDataTypes &dataSetWriters
        );
    UaWriterGroupDataType(const UaExtensionObject &extensionObject);
    UaWriterGroupDataType(const OpcUa_ExtensionObject &extensionObject);
    UaWriterGroupDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaWriterGroupDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaWriterGroupDataType();

    void clear();

    bool operator==(const UaWriterGroupDataType &other) const;
    bool operator!=(const UaWriterGroupDataType &other) const;

    UaWriterGroupDataType& operator=(const UaWriterGroupDataType &other);

    OpcUa_WriterGroupDataType* copy() const;
    void copyTo(OpcUa_WriterGroupDataType *pDst) const;

    static OpcUa_WriterGroupDataType* clone(const OpcUa_WriterGroupDataType& source);
    static void cloneTo(const OpcUa_WriterGroupDataType& source, OpcUa_WriterGroupDataType& copy);

    void attach(OpcUa_WriterGroupDataType *pValue);
    OpcUa_WriterGroupDataType* detach(OpcUa_WriterGroupDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setWriterGroupDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setWriterGroupDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setWriterGroupDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setWriterGroupDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setWriterGroupDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        OpcUa_MessageSecurityMode securityMode,
        const UaString& securityGroupId,
        const UaEndpointDescriptions &securityKeyServices,
        OpcUa_UInt32 maxNetworkMessageSize,
        const UaKeyValuePairs &groupProperties,
        OpcUa_UInt16 writerGroupId,
        OpcUa_Double publishingInterval,
        OpcUa_Double keepAliveTime,
        OpcUa_Byte priority,
        const UaStringArray &localeIds,
        const UaString& headerLayoutUri,
        UaExtensionObject& transportSettings,
        UaExtensionObject& messageSettings,
        const UaDataSetWriterDataTypes &dataSetWriters
        );

    UaString getName() const;
    OpcUa_Boolean getEnabled() const;
    OpcUa_MessageSecurityMode getSecurityMode() const;
    UaString getSecurityGroupId() const;
    void getSecurityKeyServices(UaEndpointDescriptions& securityKeyServices) const;
    OpcUa_UInt32 getMaxNetworkMessageSize() const;
    void getGroupProperties(UaKeyValuePairs& groupProperties) const;
    OpcUa_UInt16 getWriterGroupId() const;
    OpcUa_Double getPublishingInterval() const;
    OpcUa_Double getKeepAliveTime() const;
    OpcUa_Byte getPriority() const;
    void getLocaleIds(UaStringArray& localeIds) const;
    UaString getHeaderLayoutUri() const;
    UaExtensionObject getTransportSettings() const;
    UaExtensionObject getMessageSettings() const;
    void getDataSetWriters(UaDataSetWriterDataTypes& dataSetWriters) const;

    void setName(const UaString& name);
    void setEnabled(OpcUa_Boolean enabled);
    void setSecurityMode(OpcUa_MessageSecurityMode securityMode);
    void setSecurityGroupId(const UaString& securityGroupId);
    void setSecurityKeyServices(const UaEndpointDescriptions &securityKeyServices);
    void setMaxNetworkMessageSize(OpcUa_UInt32 maxNetworkMessageSize);
    void setGroupProperties(const UaKeyValuePairs &groupProperties);
    void setWriterGroupId(OpcUa_UInt16 writerGroupId);
    void setPublishingInterval(OpcUa_Double publishingInterval);
    void setKeepAliveTime(OpcUa_Double keepAliveTime);
    void setPriority(OpcUa_Byte priority);
    void setLocaleIds(const UaStringArray &localeIds);
    void setHeaderLayoutUri(const UaString& headerLayoutUri);
    void setTransportSettings(UaExtensionObject& transportSettings);
    void setMessageSettings(UaExtensionObject& messageSettings);
    void setDataSetWriters(const UaDataSetWriterDataTypes &dataSetWriters);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_WriterGroupDataType.
 *
 *  This class encapsulates an array of the native OpcUa_WriterGroupDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaWriterGroupDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaWriterGroupDataTypes
{
public:
    UaWriterGroupDataTypes();
    UaWriterGroupDataTypes(const UaWriterGroupDataTypes &other);
    UaWriterGroupDataTypes(OpcUa_Int32 length, OpcUa_WriterGroupDataType* data);
    virtual ~UaWriterGroupDataTypes();

    UaWriterGroupDataTypes& operator=(const UaWriterGroupDataTypes &other);
    const OpcUa_WriterGroupDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_WriterGroupDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaWriterGroupDataTypes &other) const;
    bool operator!=(const UaWriterGroupDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_WriterGroupDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_WriterGroupDataType* data);
    OpcUa_WriterGroupDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_WriterGroupDataType* rawData() const {return m_data;}
    inline OpcUa_WriterGroupDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setWriterGroupDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setWriterGroupDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setWriterGroupDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setWriterGroupDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setWriterGroupDataTypes(OpcUa_Int32 length, OpcUa_WriterGroupDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_WriterGroupDataType* m_data;
};

#endif // UAWRITERGROUPDATATYPE_H

