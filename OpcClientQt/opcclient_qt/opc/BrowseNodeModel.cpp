#include "OpcBrowseNodeModel.h"
#include "OpcNodeTreeItem.h"

#include <QVector>

using namespace opc;

BrowseNodeModel::BrowseNodeModel(QObject* parent)
    : QAbstractItemModel(parent)
{
    d = new _NodeTreeData;
}

BrowseNodeModel::BrowseNodeModel(const QStringList& list, QObject* parent)
    : QAbstractItemModel(parent)
{
    QVector<QVariant> varArray;
    int i, len = list.length();
    varArray.reserve(len);

    for (i = 0; i < len; i++) {
        varArray << list.at(i);
    }

    d = new _NodeTreeData(varArray);
}

BrowseNodeModel::~BrowseNodeModel()
{

}

NodeTreeItem* BrowseNodeModel::treeRoot()
{
    return &d->treeRoot;
}

int BrowseNodeModel::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return static_cast<NodeTreeItem*>(parent.internalPointer())->columnCount();
    }

    return d->treeRoot.columnCount();
}

QVariant BrowseNodeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    NodeTreeItem* item = static_cast<NodeTreeItem*>(index.internalPointer());

    return item->data(index.column());
}

Qt::ItemFlags BrowseNodeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return QAbstractItemModel::flags(index);
}

QVariant BrowseNodeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return d->treeRoot.data(section);
    }

    return QVariant();
}

QModelIndex BrowseNodeModel::index(int row, int column, const QModelIndex& parent) const
{
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }

    NodeTreeItem* parentItem;

    if (!parent.isValid()) {
        parentItem = const_cast<NodeTreeItem*>(&d->treeRoot);
    }
    else {
        parentItem = static_cast<NodeTreeItem*>(parent.internalPointer());
    }

    NodeTreeItem *childItem = parentItem->child(row);

    if (childItem) {
        return createIndex(row, column, childItem);
    }

    return QModelIndex();
}

QModelIndex BrowseNodeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }

    NodeTreeItem *childItem = static_cast<NodeTreeItem*>(index.internalPointer());
    NodeTreeItem *parentItem = childItem->parentItem();

    if (parentItem == &d->treeRoot) {
        return QModelIndex();
    }

    return createIndex(parentItem->row(), 0, parentItem);
}

int BrowseNodeModel::rowCount(const QModelIndex& parent) const
{
    const NodeTreeItem* parentItem;

    if (parent.column() > 0) {
        return 0;
    }

    if (!parent.isValid()) {
        parentItem = &d->treeRoot;
    }
    else {
        parentItem = static_cast<const NodeTreeItem*>(parent.internalPointer());
    }

    return parentItem->childCount();
}

