#ifndef NODEMODELBUILDER_H
#define NODEMODELBUILDER_H

#include "OpcBrowseNodeModel.h"
#include "NodeModelBuilderThread.h"
#include "OpcNodeTreeItem.h"

#include <QObject>
#include <QIODevice>

namespace UaClientSdk {
    class UaSession;
}

namespace opc {

class NodeModelBuilder : public QObject
{
    Q_OBJECT

public:
    explicit NodeModelBuilder(UaClientSdk::UaSession* session, QObject* parent = nullptr);
    ~NodeModelBuilder() override;

    bool buildNodeTree(NodeTreeItem* rootItem, int id);
    bool setThread(NodeModelBuilderThread* thread);

    void deleteSelf();

signals:
    void affinityRestored(QThread* fromThread);
    void finished(opc::NodeModelBuilder* that, int id, bool success);

    // internal
    void doBuildNodeTree(NodeTreeItem* rootItem, int id);

    void internalDelete();

private:
    bool readSessionNodes(QIODevice* file);
    void setupModelData(const QStringList& lines, NodeTreeItem* parent);

private slots:
    void deleteInternal();
    void nodeTreeBuild(NodeTreeItem* rootItem, int id);

private:
    UaClientSdk::UaSession* m_session;
    NodeModelBuilderThread* m_thread;
};

} // namespace

#endif // NODEMODELBUILDER_H
