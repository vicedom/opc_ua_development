#include "NodeModelBuilder.h"

#include "uasession.h"
#include "uaclientsdk.h"

#include <QCoreApplication>
#include <QThread>
#include <QBuffer>

#include <QDebug>

using namespace opc;
using namespace UaClientSdk;

NodeModelBuilder::NodeModelBuilder(UaClientSdk::UaSession* session, QObject* parent)
    : QObject(parent)
    , m_session(session)
    , m_thread(nullptr)
{
    connect(this, SIGNAL(internalDelete()), this, SLOT(deleteInternal()));
    connect(this, SIGNAL(doBuildNodeTree(NodeTreeItem*,int)), this, SLOT(nodeTreeBuild(NodeTreeItem*,int)), Qt::QueuedConnection); // jump in own thread
}

NodeModelBuilder::~NodeModelBuilder()
{
    qDebug() << "d'tor NodeModelBuilder::~NodeModelBuilder called";
}

void NodeModelBuilder::deleteSelf()
{
    emit internalDelete();
}

void NodeModelBuilder::deleteInternal()
{
    if (!m_thread) {

        emit affinityRestored(m_thread);
        deleteLater();
        return;
    }

    QThread* mainThread = qApp->thread();

    if (mainThread && mainThread->isRunning()) {

        moveToThread(mainThread);
        deleteLater();
    }

    emit affinityRestored(m_thread);
}

bool NodeModelBuilder::setThread(NodeModelBuilderThread* thread)
{
    if (!thread || m_thread) {
        return false;
    }

    m_thread = thread;
    moveToThread(m_thread);

    return true;
}


bool NodeModelBuilder::buildNodeTree(NodeTreeItem* rootItem, int id)
{
    if (!m_thread) {
        return false;
    }

    emit doBuildNodeTree(rootItem, id);
    return true;
}

void NodeModelBuilder::nodeTreeBuild(NodeTreeItem* rootItem, int id)
{
    QBuffer file;
    file.open(QIODevice::ReadWrite | QIODevice::Text);

    if (!readSessionNodes(&file)) {

        file.close();
        emit finished(this, id, false);
    }

    file.seek(0);
    QString str = file.readAll().constData();
    file.close();
    QStringList list = str.split('\n');

    setupModelData(list, rootItem);
    emit finished(this, id, true);
}

void NodeModelBuilder::setupModelData(const QStringList &lines, NodeTreeItem* parent)
{
    QVector<NodeTreeItem*> parents;
    QVector<int> indentations;
    NodeTreeItem* item = parent;
    parents << item;
    indentations << 0;

    int number = 0;

    while (number < lines.count()) {
        int position = 0;
        while (position < lines[number].length()) {

            if (lines[number].at(position) != ' ') {
                break;
            }

            position++;
        }

        const QString lineData = lines[number].mid(position).trimmed();

        if (!lineData.isEmpty()) {

            // Read the column data from the rest of the line.
            const QStringList columnStrings = lineData.split('\t', QString::SkipEmptyParts);
            QVector<QVariant> columnData;
            columnData.reserve(columnStrings.count());

            for (const QString &columnString : columnStrings) {
                columnData << columnString;
            }

            if (position > indentations.last()) {

                // The last child of the current parent is now the new parent
                // unless the current parent has no children.
                if (parents.last()->childCount() > 0) {

                    parents << parents.last()->child(parents.last()->childCount() - 1);
                    indentations << position;
                }
            }
            else {

                while (position < indentations.last() && parents.count() > 0) {

                    parents.pop_back();
                    indentations.pop_back();
                }
            }

            // Append a new item to the current parent's list of children.
            parents.last()->appendChild(new NodeTreeItem(columnData, parents.last()));
        }

        ++number;
    }
}

static bool _doBreak = false;

// const UaNodeId& startingNode
static void exploreAddressSpace(UaSession* session, QIODevice* file, UaString nodeId, unsigned int level)
{
    if (_doBreak) {
        return;
    }

    QString format("\t%1");
    UaStatus                status;
    UaString                statusError;
    UaByteString            continuationPoint;
    UaReferenceDescriptions referenceDescriptions;
    ServiceSettings         serviceSettings;
    BrowseContext           browseContext;

    UaNodeId startingNode = UaNodeId::fromXmlString(nodeId);

    UaString idString;
    OpcUa_UInt16 idNsIdx;

    /*********************************************************************
     Browse Server
    **********************************************************************/
    status = session->browse(
        serviceSettings,
        startingNode,
        browseContext,
        continuationPoint,
        referenceDescriptions);
    /*********************************************************************/

    if (status.isBad())
    {
        statusError = status.toString();
        _doBreak = true;
        return;
    }
    else
    {
        OpcUa_ReferenceDescription refDesc;
        OpcUa_UInt32 i, len = referenceDescriptions.length();

        for (i = 0; i < len; i++) {

            refDesc = referenceDescriptions[i];
            QString display(level, ' ');

            UaNodeId referenceTypeId(refDesc.ReferenceTypeId);
            display += format.arg(referenceTypeId.toString().toUtf8());
            UaQualifiedName browseName(refDesc.BrowseName);
            display += format.arg(browseName.toString().toUtf8());

            QString nodeClass;
            if (refDesc.NodeClass & OpcUa_NodeClass_Object) nodeClass = "Object";
            else if (refDesc.NodeClass & OpcUa_NodeClass_Variable) nodeClass = "Variable";
            else if (refDesc.NodeClass & OpcUa_NodeClass_Method) nodeClass = "Method";
            else if (refDesc.NodeClass & OpcUa_NodeClass_ObjectType) nodeClass = "ObjectType";
            else if (refDesc.NodeClass & OpcUa_NodeClass_VariableType) nodeClass = "VariableType";
            else if (refDesc.NodeClass & OpcUa_NodeClass_ReferenceType) nodeClass = "ReferenceType";
            else if (refDesc.NodeClass & OpcUa_NodeClass_DataType) nodeClass = "DataType";
            else if (refDesc.NodeClass & OpcUa_NodeClass_View) nodeClass = "View";
            else if (refDesc.NodeClass & OpcUa_NodeClass_Unspecified) nodeClass = "Unspecified";
            display += format.arg(nodeClass);

            UaNodeId nodeId(refDesc.NodeId.NodeId);
            display += format.arg(nodeId.toFullString().toUtf8());
            display += "\n";

            file->write(display.toUtf8().constData());

            idString = nodeId.toXmlString();
            idNsIdx = nodeId.namespaceIndex();

            exploreAddressSpace(session, file, idString, level+1);

            if (_doBreak) {
                return;
            }
        }

        // Check if the continuation point was set -> call browseNext
        while (continuationPoint.length() > 0)
        {
            /*********************************************************************
             Browse remaining nodes in the Server
            **********************************************************************/
            status = session->browseNext(
                serviceSettings,
                OpcUa_False,
                continuationPoint,
                referenceDescriptions);
            /*********************************************************************/

            if (status.isBad())
            {
                statusError = status.toString();
                _doBreak = true;
                return;
            }
            else
            {
                len = referenceDescriptions.length();

                for (i = 0; i < len; i++) {

                    refDesc = referenceDescriptions[i];
                    QString display(level, ' ');

                    UaNodeId referenceTypeId(refDesc.ReferenceTypeId);
                    display += format.arg(referenceTypeId.toString().toUtf8());
                    UaQualifiedName browseName(refDesc.BrowseName);
                    display += format.arg(browseName.toString().toUtf8());

                    QString nodeClass;
                    if (refDesc.NodeClass & OpcUa_NodeClass_Object) nodeClass = "Object";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_Variable) nodeClass = "Variable";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_Method) nodeClass = "Method";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_ObjectType) nodeClass = "ObjectType";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_VariableType) nodeClass = "VariableType";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_ReferenceType) nodeClass = "ReferenceType";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_DataType) nodeClass = "DataType";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_View) nodeClass = "View";
                    else if (refDesc.NodeClass & OpcUa_NodeClass_Unspecified) nodeClass = "Unspecified";
                    display += format.arg(nodeClass);

                    UaNodeId nodeId(refDesc.NodeId.NodeId);
                    display += format.arg(nodeId.toFullString().toUtf8());
                    display += "\n";

                    file->write(display.toUtf8().constData());

                    idString = nodeId.toXmlString();
                    idNsIdx = nodeId.namespaceIndex();

                    exploreAddressSpace(session, file, idString, level+1);

                    if (_doBreak) {
                        return;
                    }
                }
            }
        }
    }

    return;
}

bool NodeModelBuilder::readSessionNodes(QIODevice* file)
{
    _doBreak = false;

    if (!file) {
        return false;
    }

    // Root as starting node for recursive browsing
    UaNodeId startingNode(OpcUaId_RootFolder);

    UaString idString = startingNode.toXmlString();

    exploreAddressSpace(m_session, file, idString, 0);

    bool success = !_doBreak;
    _doBreak = false;
    return success;
}

