#include "OpcClientCallback.h"

#include <QCoreApplication>
#include <QThread>

#include <QDebug>

using namespace UaClientSdk;

OpcClientCallback::OpcClientCallback(QObject* parent)
    : QObject(parent)
{
    connect(this, SIGNAL(internalDelete()), this, SLOT(deleteInternal()));
}

OpcClientCallback::~OpcClientCallback()
{
    qDebug() << "d'tor OpcClientCallback::~OpcClientCallback called";
}

void OpcClientCallback::deleteSelf()
{
    emit internalDelete();
}

void OpcClientCallback::deleteInternal()
{
    QThread* mainThread = qApp->thread();

    if (mainThread && mainThread->isRunning()) {

        moveToThread(mainThread);
        deleteLater();
    }

    emit affinityRestored();
}


void OpcClientCallback::connectionStatusChanged(OpcUa_UInt32 clientConnectionId,
                                                UaClient::ServerStatus serverStatus)
{
    emit connectionStatusChangedOccured(clientConnectionId, static_cast<int>(serverStatus));
}

bool OpcClientCallback::connectError(OpcUa_UInt32 clientConnectionId,
                                     UaClient::ConnectServiceType serviceType,
                                     const UaStatus& error,
                                     bool clientSideError)
{
    uint errorVal = error.code();
    QString errorString = error.toString().toUtf8();

    emit connectErrorOccured(clientConnectionId,
                             static_cast<int>(serviceType),
                             errorVal,
                             errorString,
                             clientSideError);


    return false;
}

void OpcClientCallback::readComplete(OpcUa_UInt32 transactionId,
                                     const UaStatus& result,
                                     const UaDataValues& values,
                                     const UaDiagnosticInfos& /*diagnosticInfos*/)
{
    Q_UNUSED(values);
    Q_UNUSED(result);
    Q_UNUSED(transactionId);
}

void OpcClientCallback::writeComplete(OpcUa_UInt32 transactionId,
                                      const UaStatus& result,
                                      const UaStatusCodeArray& results,
                                      const UaDiagnosticInfos& /*diagnosticInfos*/)
{
    Q_UNUSED(results);
    Q_UNUSED(result);
    Q_UNUSED(transactionId);
}
