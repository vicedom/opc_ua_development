#ifndef UACLIENTCALLBACK_H
#define UACLIENTCALLBACK_H

#include "uaclientsdk.h"
#include "uasession.h"
#include <vector>
#include "uagenericunionvalue.h"

#include <QObject>

namespace UaClientSdk {

class OpcClientCallback : public QObject
                       , public UaSessionCallback
//                       , public UaSubscriptionCallback
{
    Q_OBJECT
public:
    explicit OpcClientCallback(QObject* parent = nullptr);
    ~OpcClientCallback() override;

    UaSessionCallback* sessionCallback()
    {
        return this;
    }
//    UaSubscriptionCallback* subscriptionCallback()
//    {
//        return this;
//    }

    void deleteSelf();

    /** Status of the server connection. */
    void connectionStatusChanged(OpcUa_UInt32 clientConnectionId,
                                 UaClient::ServerStatus serverStatus) override;

    /** Connect errors. */
    bool connectError(OpcUa_UInt32 clientConnectionId,          //!< [in] Client defined handle of the affected session
                      UaClient::ConnectServiceType serviceType, //!< [in] The failing connect step
                      const UaStatus& error,                    //!< [in] Status code for the error situation
                      bool clientSideError) override;           //!< [in] Flag indicating if the bad status was created in the Client SDK

    /** Send read results. */
    void readComplete(OpcUa_UInt32 transactionId,
                      const UaStatus& result,
                      const UaDataValues& values,
                      const UaDiagnosticInfos& diagnosticInfos) override;

    /** Send write results. */
    void writeComplete(OpcUa_UInt32 transactionId,
                       const UaStatus& result,
                       const UaStatusCodeArray& results,
                       const UaDiagnosticInfos& diagnosticInfos) override;

//    /** Send changed data. */
//    void dataChange(OpcUa_UInt32 clientSubscriptionHandle,
//                    const UaDataNotifications& dataNotifications,
//                    const UaDiagnosticInfos& diagnosticInfos) override;

//    /** Send subscription state change. */
//    void subscriptionStatusChanged(OpcUa_UInt32 clientSubscriptionHandle,
//                                   const UaStatus& status) override;

//    /** Received a KeepAliveMessage. */
//    void keepAlive(OpcUa_UInt32 clientSubscriptionHandle) override;

//    /** The client SDK detected a gap in the sequence numbers of PublishResponses received.
//     *  The client application can use the UaSubscription::republish method to request the missing notifications. */
//    void notificationsMissing(OpcUa_UInt32 clientSubscriptionHandle,
//                              OpcUa_UInt32 previousSequenceNumber,
//                              OpcUa_UInt32 newSequenceNumber) override;

//    /** Send new events. */
//    void newEvents(OpcUa_UInt32 clientSubscriptionHandle,
//                   UaEventFieldLists& eventFieldList) override;


signals:
    void affinityRestored();
    void connectionStatusChangedOccured(uint clientConnectionId,
                                        int serverStatus);
    void connectErrorOccured(uint clientConnectionId,
                             int serviceType,
                             uint error,
                             QString errorString,
                             bool clientSideError);

    void internalDelete();

private slots:
    void deleteInternal();
};

} // namespace

#endif // UACLIENTCALLBACK_H
