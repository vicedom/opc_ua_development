#include "OpcClient.h"
#include "NodeModelBuilder.h"

#include "uabase.h"
#include "uaclientsdk.h"
#include "uasession.h"

#include "uaplatformlayer.h"
#include "uadatavalue.h"
#include "uadiscovery.h"

/* low level and stack */
#include "opcua_core.h"

#if OPCUA_SUPPORT_PKI
#include "uapkicertificate.h"
#include "uapkirevocationlist.h"
#endif // OPCUA_SUPPORT_PKI

#include "uasettings.h"
#include "libtrace.h"
#include "uaeventfilter.h"
#include "uaargument.h"
#include "uacertificatedirectoryobject.h"
#include "uatrustlistobject.h"
#include <iostream>
#include <uadir.h>
#include "uaunistringlist.h"

#include <QCoreApplication>
#include <QMetaEnum>

#include <QDebug>

using namespace opc;

IOpcClient* opcClient()
{
    return OpcClient::instance();
}

OpcClient* OpcClient::m_instance = nullptr;

OpcClient* OpcClient::instance()
{
    if (!m_instance) {
        m_instance = new OpcClient;
    }

    return m_instance;
}

OpcClient::OpcClient(QObject* parent)
    : IOpcClient(parent)
    , m_session(nullptr)
    , m_sessionSecurityInfo(nullptr)
    , m_callback(nullptr)
    , m_callbackThread(nullptr)
    , m_companyName("Unknown Company")
    , m_productName("Unkown Product")
{
    m_sessionSecurityInfo = new SessionSecurityInfo;
    m_callbackThread = new ClientCallbackThread(this);
}

OpcClient::~OpcClient()
{
    qDebug() << "d'tor uac::OpcClient called";

    delete m_sessionSecurityInfo;
}

void OpcClient::callbackAffinityRestored()
{
    m_callbackThread->quit();
}

void OpcClient::OpcClient::nodeBuilderAffinityRestored(QThread* threadFrom)
{
    if (threadFrom) {

        threadFrom->quit();
        threadFrom->deleteLater();
    }
}

void OpcClient::nodeBuilderFinished(opc::NodeModelBuilder* that, int id, bool success)
{
    that->deleteSelf();

    if (!success) {
        m_activeNodeModels.remove(id);
    }

    emit sessionNodeModelBuilt(id, success);
}

QString OpcClient::companyName() const
{
    return m_companyName;
}

void OpcClient::setCompanyName(const QString& companyName)
{
    QString text = companyName.trimmed();

    if (text.isEmpty() || text == m_companyName) {
        return;
    }

    m_companyName = companyName;
    emit companyNameChanged();
}

QString OpcClient::productName() const
{
    return m_productName;
}

void OpcClient::setProductName(const QString& productName)
{
    QString text = productName.trimmed();

    if (text.isEmpty() || text == m_productName) {
        return;
    }

    m_productName = productName;
    emit productNameChanged();
}

bool OpcClient::isConnected() const
{
    return !m_serverUrl.isEmpty();
}

bool OpcClient::connect(const QString& url)
{
    QString serverUrl = url.trimmed();
    int ret = UaPlatformLayer::init();

    if (isConnected() || serverUrl.isEmpty() || ret != 0) {

        disconnect();

        if (serverUrl.isEmpty() || ret != 0) {
            return false;
        }
    }


    UaStatus status;

    UaString sUrl(qPrintable(serverUrl));
    UaString sApplicationName(qPrintable(qApp->applicationName()));
    UaString sCompanyName(m_companyName.toUtf8().constData());
    UaString sProductName(m_productName.toUtf8().constData());

    UaString sNodeName("unknown_host");
    char szHostName[256];

    if (0 == UA_GetHostname(szHostName, 256)) {
        sNodeName = szHostName;
    }

    SessionConnectInfo sessionConnectInfo;
    sessionConnectInfo.sApplicationName = sApplicationName;
    // Client_Cpp_SDK@LenovoT520
    sessionConnectInfo.sApplicationUri  = UaString("urn:%1:%2:%3").arg(sNodeName).arg(sCompanyName).arg(sProductName);
    sessionConnectInfo.sProductUri      = UaString("urn:%1:%2").arg(sCompanyName).arg(sProductName);
    sessionConnectInfo.sSessionName     = sessionConnectInfo.sApplicationUri;
    sessionConnectInfo.bAutomaticReconnect = OpcUa_True;

    m_session = new UaSession();

    m_callback = new OpcClientCallback;
    QObject::connect(m_callback, SIGNAL(affinityRestored()), this, SLOT(callbackAffinityRestored()), Qt::DirectConnection);
    QObject::connect(m_callback, SIGNAL(connectionStatusChangedOccured(uint,int)), this, SLOT(connectionStatusChanged(uint,int)));
    QObject::connect(m_callback, SIGNAL(connectErrorOccured(uint,int,uint,QString,bool)), this, SLOT(connectError(uint,int,uint,QString,bool)));

    m_callbackThread->start();
    m_callback->moveToThread(m_callbackThread);

    status = m_session->connect(
                sUrl,
                sessionConnectInfo,
                *m_sessionSecurityInfo,
                m_callback);

    if (status.isBad())
    {
        delete m_session;
        m_session = nullptr;

        m_callback->deleteSelf();
        m_callback = nullptr;

        return false;
    }

    m_serverUrl = serverUrl;

//    NodeTreeItem* testRoot = sessionRootNode();
//    QString err;
//    bool success = browselNodeTreeItemChildren(testRoot->child(1), err);
//    success = browselNodeTreeItemChildren(testRoot->child(1)->child(2), err);
//    success = browselNodeTreeItemChildren(testRoot->child(1)->child(2)->child(16), err);

//    delete testRoot;

    return true;
}

void OpcClient::disconnect()
{
    if (m_session == nullptr) {

        Q_ASSERT(m_serverUrl.isEmpty());
        return;
    }

    ServiceSettings serviceSettings;

    m_session->disconnect(
                serviceSettings,    // Use default settings
                OpcUa_True);

    delete m_session;
    m_session = nullptr;

    m_callback->deleteSelf();
    m_callback = nullptr;

    UaPlatformLayer::cleanup();

    m_serverUrl.clear();
}

NodeTreeItem* OpcClient::sessionRootNode() const
{
    if (!m_session) {
        return nullptr;
    }

    NodeTreeItem* item = new NodeTreeItem;

    UaNodeId startingNode(OpcUaId_RootFolder);

    item->nodeId = startingNode.toXmlString().toUtf8();
    item->nameSpace = startingNode.namespaceIndex();
    item->nodeClass = NodeTreeItem::Unspecified;

    QString errorString;

    if (!browselNodeTreeItemChildren(item, errorString)) {

        // error message handling !!!
        delete item;
        return nullptr;
    }

    return item;
}

bool OpcClient::browselNodeTreeItemChildren(NodeTreeItem* item, QString& errorString) const
{
    if (!m_session) {
        return false;
    }

    if (!item) {

        Q_ASSERT(false);

        errorString = "No NodeTreeItem provided";
        return false;
    }

    if (item->nodeId.trimmed().isEmpty()) {

        errorString = "NodeTreeItem: empty nodeId";
        return false;
    }

    if (item->isValue() || item->expandProceeded) {
        return true;
    }

    UaString itemNodeId = qPrintable(item->nodeId);
    UaNodeId startingNode = UaNodeId::fromXmlString(itemNodeId);

    errorString.clear();

    UaStatus                status;
    UaByteString            continuationPoint;
    UaReferenceDescriptions referenceDescriptions;
    ServiceSettings         serviceSettings;
    BrowseContext           browseContext;

    status = m_session->browse(
        serviceSettings,
        startingNode,
        browseContext,
        continuationPoint,
        referenceDescriptions);

    if (status.isBad()) {

        errorString = status.toString().toUtf8();
        return false;
    }

    OpcUa_ReferenceDescription refDesc;
    OpcUa_UInt32 i, len = referenceDescriptions.length();

    for (i = 0; i < len; i++) {

        refDesc = referenceDescriptions[i];
        UaString displayString = refDesc.DisplayName.Text;
        int nc = refDesc.NodeClass;

        UaNodeId nodeId(refDesc.NodeId.NodeId);
        UaString idString = nodeId.toXmlString();
        int nsIdx = nodeId.namespaceIndex();

        NodeTreeItem* newItem = new NodeTreeItem;

        newItem->nodeId = idString.toUtf8();
        newItem->nameSpace = nsIdx;
        newItem->displayName = displayString.toUtf8();
        newItem->nodeClass = static_cast<NodeTreeItem::NodeClass>(nc);

        item->appendChild(newItem);
    }


    while (continuationPoint.length() > 0) {

        status = m_session->browseNext(
            serviceSettings,
            OpcUa_False,
            continuationPoint,
            referenceDescriptions);

        if (status.isBad()) {

            errorString = status.toString().toUtf8();
            return false;
        }

        len = referenceDescriptions.length();

        for (i = 0; i < len; i++) {

            refDesc = referenceDescriptions[i];
            UaString displayString = refDesc.DisplayName.Text;
            int nc = refDesc.NodeClass;

            UaNodeId nodeId(refDesc.NodeId.NodeId);
            UaString idString = nodeId.toXmlString();
            int nsIdx = nodeId.namespaceIndex();

            NodeTreeItem* newItem = new NodeTreeItem;

            newItem->nodeId = idString.toUtf8();
            newItem->nameSpace = nsIdx;
            newItem->displayName = displayString.toUtf8();
            newItem->nodeClass = static_cast<NodeTreeItem::NodeClass>(nc);

            item->appendChild(newItem);
        }
    }

    item->expandProceeded = true;

    return true;
}

bool OpcClient::buildSessionNodeModel(BrowseNodeModel** modelPtr, int id)
{
    if (!modelPtr) {
        return false;
    }

    *modelPtr = nullptr;

     if (!m_session) {
         return false;
     }


    if (m_activeNodeModels.contains(id)) {
        return false;
    }

    NodeModelBuilderThread* thread = new NodeModelBuilderThread(this);
    NodeModelBuilder* builder = new NodeModelBuilder(m_session);
    QObject::connect(builder, SIGNAL(finished(opc::NodeModelBuilder*,int,bool)), this, SLOT(nodeBuilderFinished(opc::NodeModelBuilder*,int,bool)));
    QObject::connect(builder, SIGNAL(affinityRestored(QThread*)), this, SLOT(nodeBuilderAffinityRestored(QThread*)));

    thread->start();
    builder->setThread(thread);

    BrowseNodeModel* model = new BrowseNodeModel(this);
    *modelPtr = model;
    m_activeNodeModels[id] = model;
    builder->buildNodeTree(model->treeRoot(), id);

    return true;
}

void OpcClient::releaseSessionNodeModel(int id)
{
    if (!m_activeNodeModels.contains(id)) {
        return;
    }

    BrowseNodeModel* model = m_activeNodeModels.take(id);
    delete model;
}



void OpcClient::connectionStatusChanged(uint clientConnectionId,
                                        int _serverStatus)
{
    QString format(": %1");
    QString eventType = SESSION_EVENT_CONNECTION_STATUS;
    QString connectionIdString = EVENT_VAL_clientConnectionId + format.arg(clientConnectionId);

    UaClient::ServerStatus serverStatus = static_cast<UaClient::ServerStatus>(_serverStatus);
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("ServerStatus"));
    QString statusString = metaEnum.valueToKey(serverStatus);
    QString serverStatusString = EVENT_VAL_serverStatus + format.arg(statusString);

    QStringList list;
    list.append(connectionIdString);
    list.append(serverStatusString);

    QVariantHash hash;
    hash[EVENT_VAL_clientConnectionId] = clientConnectionId;
    hash[EVENT_VAL_serverStatus] = _serverStatus;

    emit sessionEvent(eventType, list, hash);
}

void OpcClient::connectError(uint clientConnectionId,
                             int _serviceType,
                             uint error,
                             QString errorString,
                             bool clientSideError)
{
    QString format(": %1");
    QString formatX(": 0x%1");
    QString eventType = SESSION_EVENT_CONNECT_ERROR;
    QString connectionIdString = EVENT_VAL_clientConnectionId + format.arg(clientConnectionId);

    UaClient::ConnectServiceType serviceType = static_cast<UaClient::ConnectServiceType>(_serviceType);
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("ConnectServiceType"));
    QString typeString = metaEnum.valueToKey(serviceType);
    QString serverTypeString = EVENT_VAL_serviceType + format.arg(typeString);

    QString errorNumberString = EVENT_VAL_error + formatX.arg(QString::number(error, 16));
    QString errorStringString = EVENT_VAL_errorString + format.arg(errorString);
    QString clientSideErrorString = EVENT_VAL_clientSideError + format.arg(clientSideError ? "true" : "false");

    QStringList list;
    list.append(connectionIdString);
    list.append(serverTypeString);
    list.append(errorNumberString);
    list.append(errorStringString);
    list.append(clientSideErrorString);

    QVariantHash hash;
    hash[EVENT_VAL_clientConnectionId] = clientConnectionId;
    hash[EVENT_VAL_serviceType] = _serviceType;
    hash[EVENT_VAL_error] = error;
    hash[EVENT_VAL_errorString] = errorString;
    hash[EVENT_VAL_clientSideError] = clientSideError;

    emit sessionEvent(eventType, list, hash);
}
