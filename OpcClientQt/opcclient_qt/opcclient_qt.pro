QT       += core network

TEMPLATE = lib

CONFIG += c++11 staticlib

CONFIG(debug, debug|release) : TARGET = $$join(TARGET,,,d)

GENERAL_LIB_DIR = "../../libs"
UA_INCLUDE_DIR = "../../UAIncludes"
UA_THIRD_PARTY_DIR = "../../UAThirdParty"
OPC_INCLUDE_DIR = "../includes"

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $${UA_INCLUDE_DIR}/uabasecpp
INCLUDEPATH += $${UA_INCLUDE_DIR}/uaclientcpp
INCLUDEPATH += $${UA_INCLUDE_DIR}/uastack
INCLUDEPATH += $${UA_INCLUDE_DIR}/uapkicpp
INCLUDEPATH += $${UA_THIRD_PARTY_DIR}/include
INCLUDEPATH += $${OPC_INCLUDE_DIR}

INCLUDEPATH += ua
INCLUDEPATH += general
INCLUDEPATH += opc

MOC_DIR     = generated
OBJECTS_DIR = generated
UI_DIR      = generated
RCC_DIR     = generated

DESTDIR = $${GENERAL_LIB_DIR}

SOURCES += \
    OpcClient.cpp \
    general/ClientCallbackThread.cpp \
    general/NodeModelBuilderThread.cpp \
    general/NodeTreeItem.cpp \
    opc/BrowseNodeModel.cpp \
    opc/NodeModelBuilder.cpp \
    ua/OpcClientCallback.cpp

HEADERS += \
    ../includes/IOpcClient.h \
    ../includes/OpcBrowseNodeModel.h \
    ../includes/OpcNodeTreeItem.h \
    ../includes/OpcTypes.h \
    ../includes/OpcDefines.h \
    OpcClient.h \
    general/ClientCallbackThread.h \
    general/NodeModelBuilderThread.h \
    opc/NodeModelBuilder.h \
    ua/OpcClientCallback.h


