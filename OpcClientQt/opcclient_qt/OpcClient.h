#ifndef UACLIENT_QT_UACLIENT_H
#define UACLIENT_QT_UACLIENT_H

#include "IOpcClient.h"
#include "ClientCallbackThread.h"


#include "uabase.h"
#include "uaclientsdk.h"

#include "OpcClientCallback.h"

#include <QVector>
#include <QHash>

namespace opc {
    class NodeModelBuilder;
}

using namespace UaClientSdk;

namespace opc {

typedef QHash<int, BrowseNodeModel*> hash_int_NodeModelStar;

class  OpcClient : public IOpcClient
{
    Q_OBJECT    

public:    
    ~OpcClient() override;

    QString companyName() const override;
    void setCompanyName(const QString& companyName) override;

    QString productName() const override;
    void setProductName(const QString& productName) override;

    bool isConnected() const override;
    bool connect(const QString& serverUrl) override;
    void disconnect() override;

    NodeTreeItem* sessionRootNode() const;

    bool buildSessionNodeModel(BrowseNodeModel** modelPtr, int id = 0) override;
    void releaseSessionNodeModel(int id) override;

    static OpcClient* instance();

private:
    explicit OpcClient(QObject* parent = nullptr);

    bool browselNodeTreeItemChildren(NodeTreeItem* item, QString& errorString) const;

private slots:
    void callbackAffinityRestored();
    void nodeBuilderAffinityRestored(QThread* threadFrom);
    void nodeBuilderFinished(opc::NodeModelBuilder* that, int id, bool success);

    void connectionStatusChanged(uint clientConnectionId,
                                 int serverStatus);
    void connectError(uint clientConnectionId,
                      int serviceType,
                      uint error,
                      QString errorString,
                      bool clientSideError);

private:
    UaSession* m_session;
    SessionSecurityInfo* m_sessionSecurityInfo;
    OpcClientCallback* m_callback;
    ClientCallbackThread* m_callbackThread;

    QString m_companyName;
    QString m_productName;
    QString m_serverUrl;

    hash_int_NodeModelStar m_activeNodeModels;

    static OpcClient* m_instance;
};

} // namespace

#endif // UACLIENT_QT_UACLIENT_H
