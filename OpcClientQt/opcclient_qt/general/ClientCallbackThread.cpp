#include "ClientCallbackThread.h"

#include <QDebug>

ClientCallbackThread::ClientCallbackThread(QObject* parent)
    : QThread(parent)
{

}

ClientCallbackThread::~ClientCallbackThread()
{
    qDebug() << "d'tor ClientCallbackThread::~ClientCallbackThread called";
}
