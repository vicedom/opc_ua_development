#ifndef NODEMODELBUILDERTHREAD_H
#define NODEMODELBUILDERTHREAD_H

#include <QThread>

class NodeModelBuilderThread : public QThread
{
    Q_OBJECT
public:
    explicit NodeModelBuilderThread(QObject *parent = nullptr);
    ~NodeModelBuilderThread() override;

signals:

public slots:
};

#endif // NODEMODELBUILDERTHREAD_H
