#ifndef CLIENTCALLBACKTHREAD_H
#define CLIENTCALLBACKTHREAD_H

#include <QThread>

class ClientCallbackThread : public QThread
{
    Q_OBJECT
public:
    explicit ClientCallbackThread(QObject* parent = nullptr);
    ~ClientCallbackThread() override;
};

#endif // CLIENTCALLBACKTHREAD_H
