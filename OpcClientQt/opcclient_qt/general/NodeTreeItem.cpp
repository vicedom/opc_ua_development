#include "OpcNodeTreeItem.h"

using namespace opc;

NodeTreeItem::NodeTreeItem(NodeTreeItem *parent)
    : m_parentItem(parent)
    , nodeClass(Invalid)
    , nameSpace(-1)
    , expandProceeded(false)
{

}

NodeTreeItem::NodeTreeItem(const QVector<QVariant> &data, NodeTreeItem *parent)
    : m_itemData(data)
    , m_parentItem(parent)
    , nodeClass(Invalid)
    , nameSpace(-1)
    , expandProceeded(false)
{

}

NodeTreeItem::~NodeTreeItem()
{
    qDeleteAll(m_childItems);
}

void NodeTreeItem::appendChild(NodeTreeItem *item)
{
    m_childItems.append(item);
}

bool NodeTreeItem::isValid() const
{
    return nameSpace >= 0 && !nodeId.trimmed().isEmpty();
}

bool NodeTreeItem::isValue() const
{
    return nodeClass == Variable;
}

NodeTreeItem *NodeTreeItem::child(int row)
{
    if (row < 0 || row >= m_childItems.size()) {
        return nullptr;
    }

    return m_childItems.at(row);
}

int NodeTreeItem::childCount() const
{
    return m_childItems.count();
}

int NodeTreeItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<NodeTreeItem*>(this));

    return 0;
}

int NodeTreeItem::columnCount() const
{
    return m_itemData.count();
}

QVariant NodeTreeItem::data(int column) const
{
    if (column < 0 || column >= m_itemData.size())
        return QVariant();
    return m_itemData.at(column);
}

NodeTreeItem *NodeTreeItem::parentItem()
{
    return m_parentItem;
}
