#include "MainDialog.h"
#include "ui/ViewConnectionStateDialog.h"

#include "ui_MainDialog.h"

MainDialog::MainDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainDialog)
    , m_serverUrlTextSet(false)
    , m_readIdTextSet(false)
    , m_writeIdTextSet(false)
    , m_monitorIdTextSet(false)
    , m_nodeModel(nullptr)
{
    setWindowFlags(Qt::Window
                 | Qt::CustomizeWindowHint
                 | Qt::WindowTitleHint
                 | Qt::WindowSystemMenuHint
                 | Qt::WindowMinMaxButtonsHint
                 | Qt::WindowCloseButtonHint);

    ui->setupUi(this);

#ifdef _DEVELOPMENT_INTERNAL_USE
    ui->selectServerComboBox->insertItem(0, "opc.tcp://localhost:48010");
    ui->selectServerComboBox->setCurrentIndex(0);
#endif

    connect(OPC, SIGNAL(sessionEvent(QString,QStringList,QVariantHash)), this, SLOT(onSessionEvent(QString,QStringList,QVariantHash)));

    OPC->setCompanyName(COMPANY_NAME);
    OPC->setProductName(PRODUCT_NAME);
}

MainDialog::~MainDialog()
{
    delete ui;
}

PROP_DEFINE_STRING_SET(serverUrlText, ServerUrlText)
void MainDialog::enableServerUrlTextSet()
{
    ui->connectButton->setEnabled(serverUrlTextSet());
}

PROP_DEFINE_STRING_SET(readIdText, ReadIdText)
void MainDialog::enableReadIdTextSet()
{
    bool enable = m_serverStatus == opc::IOpcClient::Connected && readIdTextSet();

    ui->readIdSetAllButton->setEnabled(enable);
    ui->readSynchronousButton->setEnabled(enable);
    ui->readAsynchronousButton->setEnabled(enable);
}

PROP_DEFINE_STRING_SET(writeIdText, WriteIdText)
void MainDialog::enableWriteIdTextSet()
{
    bool enable = m_serverStatus == opc::IOpcClient::Connected && writeIdTextSet();

    ui->writeIdValueEdit->setEnabled(enable);
    ui->writeIdSetAllButton->setEnabled(enable);
    ui->writeSynchronousButton->setEnabled(enable);
    ui->writeAsynchronousButton->setEnabled(enable);
}

PROP_DEFINE_STRING_SET(monitorIdText, MonitorIdText)
void MainDialog::enableMonitorIdTextSet()
{
    bool enable = m_serverStatus == opc::IOpcClient::Connected && monitorIdTextSet();

    ui->monitorIdSetAllButton->setEnabled(enable);
    ui->monitorStartStopButton->setEnabled(enable);
}

PROP_DEFINE_STRING_SET(connectionStateText, ConnectionStateText)
void MainDialog::enableConnectionStateTextSet()
{
    ui->viewConnectionStateButton->setEnabled(connectionStateTextSet());
}

PROP_DEFINE_TYPE(serverStatus, ServerStatus, opc::IOpcClient::ServerStatus)
void MainDialog::enableServerStatusSet()
{
    bool isConnected = m_serverStatus == opc::IOpcClient::Connected;

//    if (isConnected && !m_nodeModel) {
//        OPC->buildSessionNodeModel(&m_nodeModel, 4711);
//    }

    ui->actualConnectionStateLabel->setText(isConnected ? "CONNECTED" : "DISCONNECTED");
    ui->connectButton->setText(isConnected ? "Disconnect" : "Connect");
    ui->readIdSelectComboBox->setEnabled(isConnected);

    ui->readIdSelectComboBox->setEnabled(isConnected);
    ui->writeIdSelectComboBox->setEnabled(isConnected);
    ui->monitorIdSelectComboBox->setEnabled(isConnected);

    enableReadIdTextSet();
    enableWriteIdTextSet();
    enableMonitorIdTextSet();
}

void MainDialog::reject()
{
    OPC->disconnect();
    QDialog::reject();
}

void MainDialog::toggleConnect()
{
    if (OPC->isConnected()) {

        OPC->releaseSessionNodeModel(4711);
        m_nodeModel = nullptr;

        OPC->disconnect();
    }
    else {

        OPC->releaseSessionNodeModel(4711);
        m_nodeModel = nullptr;

        OPC->connect(m_serverUrlText);
    }
}

void MainDialog::onSessionEvent(const QString type, const QStringList& description, const QVariantHash &dataHash)
{
    if (type == SESSION_EVENT_CONNECTION_STATUS) {
        onSessionConnectionStatusChangedEvent(description, dataHash);
    }
    else if (type == SESSION_EVENT_CONNECT_ERROR) {
        onSessionConnectErrorEvent(description, dataHash);
    }


}

void MainDialog::viewConnectionState()
{
    ViewConnectionStateDialog dlg;
    dlg.setText(connectionStateText());

    dlg.exec();
}

void MainDialog::readSynchronous() {  }

void MainDialog::readAsynchronous() {  }

void MainDialog::writeSynchronous() {  }

void MainDialog::writeAsynchronous() {  }

void MainDialog::monitor() {  }

void MainDialog::simulate() {  }

void MainDialog::readIdSetAll() {  }

void MainDialog::writeIdSetAll() {  }

void MainDialog::monitorIdSetAll() {  }

void MainDialog::onSessionConnectionStatusChangedEvent(const QStringList& description, const QVariantHash& dataHash)
{
    connectionStateTextChanged(description.join('\n'));

    QVariant varServerStatus = dataHash.value(EVENT_VAL_serverStatus);
    opc::IOpcClient::ServerStatus serverStatus = static_cast<opc::IOpcClient::ServerStatus>(varServerStatus.toInt());

    setServerStatus(serverStatus);
}

void MainDialog::onSessionConnectErrorEvent(const QStringList& description, const QVariantHash& /*dataHash*/)
{
    connectionStateTextChanged(description.join('\n'));

    setServerStatus(opc::IOpcClient::Disconnected);
}

