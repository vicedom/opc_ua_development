#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include "IOpcClient.h"

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

#define COMPANY_NAME "Techdrivers"
#define PRODUCT_NAME "BasicClient"

#define CLASS_NAME MainDialog
#include "PropMakros.inc"

class MainDialog : public QDialog
{
    Q_OBJECT

    Q_PROPERTY(QString serverUrlText READ serverUrlText WRITE setServerUrlText)
    Q_PROPERTY(bool serverUrlTextSet READ serverUrlTextSet WRITE setServerUrlTextSet)

    Q_PROPERTY(QString readIdText READ readIdText WRITE setReadIdText)
    Q_PROPERTY(bool readIdTextSet READ readIdTextSet WRITE setReadIdTextSet)

    Q_PROPERTY(QString writeIdText READ writeIdText WRITE setWriteIdText)
    Q_PROPERTY(bool writeIdTextSet READ writeIdTextSet WRITE setWriteIdTextSet)

    Q_PROPERTY(QString monitorIdText READ monitorIdText WRITE setMonitorIdText)
    Q_PROPERTY(bool monitorIdTextSet READ monitorIdTextSet WRITE setMonitorIdTextSet)

    Q_PROPERTY(QString connectionStateText READ connectionStateText WRITE setConnectionStateText)
    Q_PROPERTY(bool connectionStateTextSet READ connectionStateTextSet WRITE setConnectionStateTextSet)

    Q_PROPERTY(opc::IOpcClient::ServerStatus serverStatus READ serverStatus WRITE setServerStatus)

public:
    MainDialog(QWidget *parent = nullptr);
    ~MainDialog();

    PROP_DECLARE_STRING_SET(serverUrlText, ServerUrlText)
    PROP_DECLARE_STRING_SET(readIdText, ReadIdText)
    PROP_DECLARE_STRING_SET(writeIdText, WriteIdText)
    PROP_DECLARE_STRING_SET(monitorIdText, MonitorIdText)
    PROP_DECLARE_STRING_SET(connectionStateText, ConnectionStateText)
    PROP_DECLARE_TYPE(serverStatus, ServerStatus, opc::IOpcClient::ServerStatus)

    void reject() override;

private:
    void onSessionConnectionStatusChangedEvent(const QStringList& description, const QVariantHash& dataHash);
    void onSessionConnectErrorEvent(const QStringList& description, const QVariantHash& dataHash);

private slots:
    void toggleConnect();
    void viewConnectionState();
    void readSynchronous();
    void readAsynchronous();
    void writeSynchronous();
    void writeAsynchronous();
    void monitor();
    void simulate();
    void serverUrlTextChanged(const QString& text);
    void readIdTextChanged(const QString& text);
    void writeIdTextChanged(const QString& text);
    void monitorIdTextChanged(const QString& text);
    void readIdSetAll();
    void writeIdSetAll();
    void monitorIdSetAll();
    void connectionStateTextChanged(const QString& text);

    void onSessionEvent(const QString type, const QStringList& description, const QVariantHash& dataHash);

private:
    Ui::MainDialog *ui;
    opc::BrowseNodeModel* m_nodeModel;

};
#endif // MAINDIALOG_H
