#include "ViewConnectionStateDialog.h"
#include "ui_ViewConnectionStateDialog.h"

ViewConnectionStateDialog::ViewConnectionStateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewConnectionStateDialog)
{
    setWindowFlags(Qt::Dialog
                 | Qt::CustomizeWindowHint
                 | Qt::WindowTitleHint
                 | Qt::WindowSystemMenuHint
                 | Qt::WindowCloseButtonHint);

    ui->setupUi(this);
}

ViewConnectionStateDialog::~ViewConnectionStateDialog()
{
    delete ui;
}

void ViewConnectionStateDialog::setText(const QString& text)
{
    ui->connectionStateTextEdit->setPlainText(text);
}
