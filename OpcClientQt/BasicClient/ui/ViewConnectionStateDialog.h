#ifndef VIEWCONNECTIONSTATEDIALOG_H
#define VIEWCONNECTIONSTATEDIALOG_H

#include <QDialog>

namespace Ui {
class ViewConnectionStateDialog;
}

class ViewConnectionStateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ViewConnectionStateDialog(QWidget *parent = nullptr);
    ~ViewConnectionStateDialog() override;

    void setText(const QString& text);

private:
    Ui::ViewConnectionStateDialog *ui;
};

#endif // VIEWCONNECTIONSTATEDIALOG_H
