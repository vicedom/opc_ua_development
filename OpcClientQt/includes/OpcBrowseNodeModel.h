#ifndef NODEMODEL_H
#define NODEMODEL_H

#include "OpcNodeTreeItem.h"

#include <QStringList>
#include <QAbstractItemModel>
#include <QModelIndex>

namespace opc {

class BrowseNodeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit BrowseNodeModel(QObject* parent = nullptr); // default header data ("Title", "Summary")
    explicit BrowseNodeModel(const QStringList& list, QObject* parent = nullptr); // sets header data
    ~BrowseNodeModel() override;

    NodeTreeItem* treeRoot();

    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

private:
    NodeTreeData d;
};

} // namespace

#endif // NODEMODEL_H
