#ifndef OPCDEFINES_H
#define OPCDEFINES_H

#define SESSION_EVENT_CONNECTION_STATUS "connectionStatusChanged"
#define SESSION_EVENT_CONNECT_ERROR "connectError"

#define EVENT_VAL_clientConnectionId "clientConnectionId"   // uint
#define EVENT_VAL_serverStatus "serverStatus"               // int (opc::IOpcClient::ServerStatus)
#define EVENT_VAL_serviceType "serviceType"                 // int (opc::IOpcClient::ConnectServiceType)
#define EVENT_VAL_error "error"                             // uint
#define EVENT_VAL_errorString "errorString"                 // QString
#define EVENT_VAL_clientSideError "clientSideError"         // bool

#endif // OPCDEFINES_H
