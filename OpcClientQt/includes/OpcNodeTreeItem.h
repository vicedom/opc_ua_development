#ifndef TREEITEM_H
#define TREEITEM_H

#include <QString>
#include <QVariant>
#include <QVector>
#include <QSharedDataPointer>

namespace opc {

class NodeTreeItem
{
public:
    /// An enumeration that identifies a NodeClass.
    enum NodeClass
    {
        Invalid       = 0xff,
        Unspecified   = 0x00,
        Object        = 0x01,
        Variable      = 0x02,
        Method        = 0x04,
        ObjectType    = 0x08,
        VariableType  = 0x10,
        ReferenceType = 0x20,
        DataType      = 0x40,
        View          = 0x80,
    };

public:
    explicit NodeTreeItem(NodeTreeItem* parentItem = nullptr);
    explicit NodeTreeItem(const QVector<QVariant>& data, NodeTreeItem* parentItem = nullptr);
    ~NodeTreeItem();

    void appendChild(NodeTreeItem *child);
    bool isValid() const;
    bool isValue() const;

    NodeTreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    NodeTreeItem *parentItem();

private:
    QVector<NodeTreeItem*> m_childItems;
    QVector<QVariant> m_itemData;
    NodeTreeItem* m_parentItem;

public:

    QString nodeId;
    NodeClass nodeClass;
    QString displayName;
    int nameSpace;
    bool expandProceeded;
    QVariant value;   // return value

};

class _NodeTreeData : public QSharedData
{
public:
      _NodeTreeData()
          : treeRoot(NodeTreeItem({ "Title", "Summary" }))
      {
      }
      _NodeTreeData(const QVector<QVariant>& data)
          : treeRoot(data)
      {
      }
      _NodeTreeData(const _NodeTreeData& other)
          : QSharedData(other)
          , treeRoot(other.treeRoot)
      {
      }
      ~_NodeTreeData() { }

      NodeTreeItem treeRoot;
};

typedef QSharedDataPointer<_NodeTreeData> NodeTreeData;

} // namespace

#endif // TREEITEM_H
