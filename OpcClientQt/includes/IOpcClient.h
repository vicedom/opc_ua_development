#ifndef IOPCCLIENT_H
#define IOPCCLIENT_H

#include "OpcTypes.h"
#include "OpcBrowseNodeModel.h"
#include "OpcDefines.h"

#include <QObject>
#include <QStringList>

#define OPC opcClient()

namespace opc {

class IOpcClient : public QObject
{
    Q_OBJECT
    Q_ENUMS(ServerStatus)
    Q_ENUMS(ConnectServiceType)

public:
    /// OPC server status enumeration used to indicate the connection status
    enum ServerStatus
    {
        Disconnected, /*!< The connection to the server is deactivated by the user of the client API. */
        Connected, /*!< The connection to the server is established and is working in normal mode. */
        ConnectionWarningWatchdogTimeout,  /*!< The monitoring of the connection to the server indicated a potential connection problem. */
        ConnectionErrorApiReconnect, /*!< The monitoring of the connection to the server detected an error and is trying to reconnect to the server. */
        ServerShutdown, /*!< The server sent a shut-down event and the client API tries a reconnect. */
        NewSessionCreated /*!< The client was not able to reuse the old session and created a new session during reconnect. This requires to redo register nodes for the new session or to read the namespace array. */
    };

    /// Service type enumeration used to indicate a connection establishment step
    enum ConnectServiceType
    {
        CertificateValidation, /*!< Certificate validation steps. */
        OpenSecureChannel, /*!< Processing of Service OpenSecureChannel. */
        CreateSession, /*!< Processing of Service CreateSession. */
        ActivateSession /*!< Processing of Service ActivateSession. */
    };

public:
    ~IOpcClient() override
    {
    }

    virtual QString companyName() const = 0;
    virtual void setCompanyName(const QString& companyName) = 0;
    virtual QString productName() const = 0;
    virtual void setProductName(const QString& productName) = 0;

    virtual bool isConnected() const = 0;
    virtual bool connect(const QString& serverUrl) = 0;
    virtual void disconnect() = 0;

    // syncronous model development
    virtual NodeTreeItem* sessionRootNode() const = 0;

    // asyncronous full model creation
    virtual bool buildSessionNodeModel(BrowseNodeModel** modelPtr, int id = 0) = 0; // returns false if BrowseNodeModel with id is allready built and not released
    virtual void releaseSessionNodeModel(int id) = 0; // model are NOT automaically released if session is disconnected (automatic reconnection)

signals:
    void companyNameChanged();
    void productNameChanged();
    void sessionEvent(const QString type, const QStringList& description, const QVariantHash& dataList);
    void sessionNodeModelBuilt(int id, bool success);
    void releasingNodeModel(int id);

protected:
    explicit IOpcClient(QObject* parent)
        : QObject(parent)
    {
    }
};

} // namespace

opc::IOpcClient* opcClient();


#endif // IOPCCLIENT_H
